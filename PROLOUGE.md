![close combat](img/close.jpg)

# USLCS V3 Prologue

The Ultimate Space Lego Character Sim is one of the best LEGO table top role playing games out there! Based on [Powered By the Apocalypse](http://apocalypse-world.com/pbta/games/find), you can play this traditionally with a Game Master, cooperatively without a GM, or even by yourself. 

Comments, questions, stories, or accusations? Create an issue! Got a great addition or rule change? Fork this repository and make a pull request!

# A History

In 1994 my little grade school self went on the internet for the very first time. The teacher showed us how to search for websites. My teacher told us to go to a special website called a search engine, in this case [WebCrawler](https://en.wikipedia.org/wiki/WebCrawler). We were to search up a topic we were interested in. I carefully typed in L, E, G, O and pressed the search button. There were *hundreds* of websites. One I found, a table top game called LEGO Wars. It was a table top war game much like Warhammer, but with LEGOs(1). My brother and I fell in love with it.

In middle and high school we adapted our own version in 1999. Still unfamiliar with Warhammer and Dungeons and Dragons, we pretentiously called it the Ultimate Space Lego Combat Simulation, or USLCS. We improved it to a second version. It was very unbalanced. Vehicles ruled the day, but by our age we were using the game as a means to continue the stories of our mini-fig characters.


(1) Yes, I know the official plural for LEGO. 10 year old me knows better.


# The Old USLCS V2 Prologue.

The Ultimate Space Lego Combat Sim is one of the best Lego war games out there.  It uses a modular rule system where a player can go as far as he or she desires into complexity.  It has a good rule system that's easy to learn and fun to play with.  It is a lot similar to the famed Warhammer 20K, but fully customizable depending on the number of Legos you own and a lot easier and funner to play.

Please note that hereafter I use the acronym instead of the full name.  This is because we don't want to have any sort of trouble with Lego Company or whatever they are called, so we can limit the number of time the word 'Lego' appears.  Legos is not our invention.  You can use any type of building brick you want for this game, it's just we use 'Lego' because it is the most popular and the best.

We hope you like this game.  If you have any comments, stories, suggestions, e-mail us at seafroggys@yahoo.com or croxisv@icqmail.com (No we don't use these anymore ;) ).  If you want some strategies to help you if you are stuck, e-mail us your problem and we'll give you some hints.

Next - Gameplay
