
Book 4: Army Level Combat
Previous - Pirate Gangs    Next - Campaign Rules

Example Nations

Army level Combat introduces a whole new dimension to the gaming of USLCS.  It is recommended that you play all the books to Level 3 or 4.  This book has a whole new concept;  player specific clans.

Player Specific Clans, or PLANS, is where YOU, the player, can design a special nation, company, or pirate gang, depending on who you play.  In your specific clan, you have special technologies, weapons, troopers, and tactics that best suit the special technology.  If you have any clan you'd like to post on the web, e-mail it to me at seafroggys@yahoo.com.  I will modify it to make it play-balanced, since the common hunch is to make your nation super strong.  My standard nation is already posted, as well as my brother Croxis'.

Democratic Empire

Type:  Nation
Government:  Feudalistic government
Ruler:  Emperor Johaan
Created and Submitted by:  J. G. Radford
Notes:  One of the strongest nations in the galaxy, the Empire has ruled a total of 16 worlds before it's famous invasion of Nimbus 3.  Now it faces a deadly enemy from within, the Corperations, which killed the great Emperor Maki.  It also faces the super strong Immortal Federation.
History:

When King Toutsky was crowned emperor of the Trojan protectorate of Plymouth Alpha, he began a large colinizing campaign.
Forgan 3 and Lunis 3 soon became colonies of the ever expanding Trojan Kingdom. Around 2412, Lunis 3 revolted, and
invaded Plymouth Alpha. This political outrage caused the Trojan government to collapse, sending the four worlds into disorder
and causing the citizens of Plymouth Alpha to flee. They headed to a planet in the Centauri system. Lunis 3 was forgotten, and
Forgan 3 and Capital Centauri formed the Democratic Empire.

This empire quickly expanded, building a large military, until the galactic war started in the 2630s. The empire was ready, and
conquered a half a dozen systems over the next 150 years. Now a very prosperous nation, the empire began going into the
scienses, using stolen technology from the other nations. This led to the discovery of several new weapons. At about the brink
of the 31'st century, the empire started again, and in 45 years expanded to what it is the current date. Several corperations
forced the emperor to sell data to the diminishing Garn Guild, causing them to grow back to the threat they were. These
corperations soon began mayham in the political courts of Capital Centauri, but luckily it was time to test their new weapon; the
anti-matter missile. They used it on the highest populated Garn world, sending about 4 billion mini-figs to their deaths. This
atrocity forced these corperations to sell data to other nations, to help develope a 'gang-up' situation against the Democratic
Empire. The core government was about to implode.

The invasion of Nimbus 7 helped the empire get back in it's tracks (the above map is about 40 years old). The Garn Guild then
collapsed, starting a civil war, in which the corperations tried to make them come back together again. The empror wouldn't
take any chances, so he began attacking any corperation vessel within restricted zones in imperial space. But these corperations
were well-equipped, and caused mayhem on the worlds listed in yellow on the map (these were changed by someone who
broke into the archives).

Imperial Vehicles:  The Empire loves using smaller, more numerous tanks to overwhelm the enemy.  Vehicle size must be at most 100 dots in area.  There is, however, no point limitation.

Imperial Troopers:  The Empire uses two special troopers.  One is an enhanced version of the Android, called the White Drone (the Empire calls it's standard androids Red Drones).  The Black Corps, a special mysterious unit that calls on espinage, covert, spying, etc., has it's own special trooper called a Black Operative, or just BlackOp.  They act like normal commandos, but they have two advantages; they have a +1 on their skill roll using sniper rifles, and all weapons targetting them has a -1 penalty to the skill roll, due to the nature of a black uniform.  A White Drone also has built-in communication.
 
Name 	White Drone
Type 	Special Android
Move 	6"
Skill 	1d10
Armor 	13
Points 	20
Name 	BlackOp
Type 	Special Infantry
Move 	6"
Skill 	1d8
Armor 	7
Points 	15

Tactics:  The Empire uses simple tactics on offense.  It sends it's troopers forward to lure the main force towards crushing them.  In the meantime, walkers and flyers do hit-and-run strikes, softening up their forces.  When their main gravship and tank force heads up to the front, with concentrated firepower from Infantry DAD guns, the enemy vehicles are stomped.  A combination of infantry pistols and rifles, vehicle flamethrowers, and artillery, the remainnig troopers of the enemy can finally be defeated.

Defensive is on the other hand, difficult.  There are several vehicles, or if there is very rough terrain, several squads of troops guarding the base.  They act as point, luring the onslaught toward them while scoring hits.  Then, when in range, the base cannons pound at any large 'base killers'.  Once all the base killers are destroyed, the infantry guards inside the base rush out and help against the oncoming infantry lines.  This countinues until the game is won or loss.

The Starwaymen

Type:  Pirates
Government:  Hierarchial Dictatorship
Ruler:  Shipking Alfred
Created and Submitted by:  J. G. Radford
Notes:  A feared pirate gang.  The Starwaymen have raided nations from the meager Doctane Republic to the vast Immortal Federation, and countless corperations.  It has three bases across two asteroid fields in the Solstist and Franklin Systems.
History:

The 'Super' gang known as the King's Might, had the monopoly on terroism, strikes on innocent places, and other atrocities.  Even the great fleets of the Banjar Republic failed to their might, and Lord Ethser was pronounced king of the Neo-Banjar Empire.  The King's Might, now ruled by Lord Ethser III, son of the Banjar's king, was under their protectorite, until Ethser attacked a refuge ship that was carrying colonists from the Banjar world of Tommoras that was invaded by the Democratic Empire.  Ethser II was so enraged that he sent his newly formed fleet to destroy his son's folly.  The King's Might fought well, yet they lost over a third of their forces and had to leave Banjar space.

Now weakened, several of the gang's members were planning to rule the all powerful pirate gang.  At least three of a noticable size arisen.  One was the Starwaymen, who was led by Ship Lieutenate Garsh of the King's Might ship Notty Man.  His forces came close to killing Ethser, but his guards prevented the assasination, and Garsh was forced to flee.  His craft was destroyed fleeing from the gang's base.  However, these three gangs would soon remove the King's Might's monopoly.

Garsh's second-in-command, Peceaut, became the first Shipking of the newly formed Starwaymen, and Garsh was forever known thereafter as the Shipfounder.  In a secret base hidden away in the Solstist system, on the outlying borders of the Banjar nation, he slowly built up his military might.  With a reputation of death and destruction in the Banjar invasion, Garsh was a well-liked and honored man in the pirate world, and people from all around joined the ranks of his gang.  Their powers grew, and as their powers grew they could raid bigger, better things, and thus get better materials to build better ships.  Finally, their first Fleet, Star I, was built.  In revenge for the death of the Shipfounder, Peceaut led this decent fleet on a hit-and-run mission against Ethser III's flagship, anchored in the Franklin System, then the home of the King's Men.  They too were remarshelling their forces after their defeat against the Banjar forces.  But this time, their flagship was destroyed.  In the process, Peceaut's ship was blasted apart too.  It was a day of sorrow for the super gang, and victory and sadness for the Starwaymen.  Navigator Alfred was then promoted to Shipking.

Now with the reputation of killing the Lord of the biggest pirate band, many more people flocked to the Starwaymen.  Now is the current date; after 30 years since the first Banjar invasion.  What further threat does the King's Might have for them, as well as the two other bands that was formed the same way they were?

Starwaymen Fleets:  It's customary when building attack fleets to have large starships hang above the battle field, covering the smaller guncraft and motorcycles that attack the base.  There must be one mobile base on the field, or two flyers over 100 dots.

Starwaymen Troopers:  The Starwaymen do have some special units of their own, mostly following their customs and religions or stolen from the King's Might.  One is the Shiplieutenate, who commands the attack fleets and is the underlings of the Shipking, and the other is the Starofficer, who is a special soldier of the Shipking's forces.  There must be at least one Shiplieutenate at the start of the battle, but not at the end.  Starofficers have the cunning ability to set a course into a gunship, and let the gunship go to that destination without a driver.  No one knows how this is done.
 
Name 	Shiplieutenate*
Type 	Fleet Commander
Move 	5"
Skill 	1d8
Armor 	6
Points 	15
* If you want to use the Shipking, increase all stats by one and make a hero.  Only one per battle.
 
Name 	Starofficer
Type 	Special Infantry
Move 	6"
Skill 	1d6+1
Armor 	5
Points 	12

Starwaymen Tactics:  Offensive tactics; Starwaymen, as mentioned above, harness their fleets over the attack zone, providing cover fire for their troops and blasting fortifacations and formations apart.  In addition to that, the Gunships are usually armed with one Starofficer each.  These gunships strafe apart infantry and small vehicles, and ward away attacks that normally would go to the big starships hanging over them.  Sometimes the gunships are supported by bikes, but due to the immense vulneralbility those are usually used on rough terrain.

Defensive bases; if they are ever attacked, they will fight to the last.  The base in mostly underground, so bombardment from ships and basekillers won't be a problem.  However, in case of infantry attack, there are usually small traps rigged all around the base, making it hazardous for ground attack.  To ward of the main attack force, interceptors and base guns are used.

The Dominion of Opia

Type:  Nation
Government:  Confederacy
Ruler:  President Milton Graves
Created and Submitted by:  J. G. Radford
Notes:  One of the newcomers to the galactic war, Opia is a rather perfectionist nation, mostly building up its innards instead of fighting other nations for new lands.  Even though it doesn't compare at all with massive nations such as the Immortal Federation, the Tri-Angalic Union, or the Democratic Empire, it is a major power to contend with.
History:

The Banjar Republic set up a trading post on the world of Opia, which is Banjarian for Future.  Many people came to live there and thus work there as dockers, farmers, businessmen, and the like.  For ten years Opia was listed as one of the top five Utopian planets in the known galaxy.  That all ended, though, when pirates began smashing the base's prosperity into smithereens.  Banjar sent military forces there to help protect the base.  At that time the Garn Guild invaded Banjar, and Opia was left on its own.

Agrian, the military governor of the colony, took control of all operations and began building up a new nation.  In no time factories were built, manufacturing many weapons of war, and sold these weapons on the open market.  Profit sky rocketed, starting a second period of prosperity.  Now with all the money in the world, Opia paid mercinaries from across the the sector to help them expand.  The nearby worlds were mostly independent, mainly from the recent incident with Garn and Banjar, some have just grown on its own for many years.  This made it very easy for Opia to conquer.  With even more worlds at their disposal, the Opians could construct far more units of war and conquer even more worlds.  But neighboring nations saw this as a threat, thus Opia began building its infrastructure to defend from a possible vastly suprior force.

At first, Garn attacked Opia, but unlike before their unsuccessful invasion of Banjar they were easily pushed back (and just that next year, the Democratic Empire came in a completly conquered them).  Then Banjar began pestering them to return Opia back to their control, but they were having a massive problem with the King's Men, so that didn't last long.  Then Mercinaries hired by the Immortal Federation, a very rich nation, attacked the Dominion.  This was a massive threat, since these were tough mercinaries, even tougher then the ones Opia hired years before.  Opia lost many lives, thus had to back away from Maxum 5.  Then another colony, Drake 3, was wiped out by a nova, which some speculated was planned.  Now eager to reconquer, Opia went on a full rampage.  They nearly annhilated the Vesus Faction, and conquered several more independent worlds.  At that time, the Immortal Federation declared open war verses them, and now they are bitter enemies.

Dominion Technology

Dominion tactics make more use out of massive bases rather then massive armies.  To protect their bases, Opian scientists have developed Sub-atomic Particle Forceshield technology.  Other nations have been working on shields for years, but the power needed to generate such technology was too much.  The SAPFT works by having 'Output' jets built into the ground.  These jets are placed are around the base, much like a fense.  These jets are connected to a central generator, which sends waves of super-heated atoms through the jets and up to the sky.  These atoms are so close together, that any form of mass, even individual atoms, cannot pass through the 'shield'.  And because the atoms are constantly being replaced, there is no way of destroying the field without destroying the generator.  One disadvantage, though, is that the generator uses mass to create the sub-atomic 'shielding', thus material must be mined in vast quantities.  The digger is built into the generator.
 
Object 	Armor 	Points
Jet Placement 	N/A 	5
Generator 	30 Armor 	100

Jet Placement is placed in 3 inch intervals, and you would place it in a line.  The line can curve, be straight, bend, whatever.  No unit can pass this line nor any gun blast can go through it.  If a unit attempts to go through this line, it is considered a collision.  Once the generator is destoryed, the 'shield' no longer exisists, and the game may follow normally, ignoring these lines.  Anybody many still orbdrop on the other side of a placement.

Dominion Troopers

Opia deployes special troopers to fit with the style of base defense they use.  In addition to their SAPF technology, the Dominon also has counter-shields that neutralizes the atoms used in the shielding.  A special trooper type, called a Shielded Trooper, has a special shielding that breaks up the material used in SAPFT shielding on contact, thus the trooper can move through it as if it wasn't even there.  Also, the Shielded Trooper gets a small armor bonus from the shield.  Also, Opian bootcamps train superb troopers to act as sharpshooters, being able to hit targets at longer ranges.
 
Name 	Shielded Trooper
Type 	Increased Mobility Trooper
Move 	5"
Skill 	1d6
Armor 	7
Points 	12
Name 	Sharpshooter
Type 	Long-ranged attack trooper
Move 	5"
Skill 	1d8
Armor 	5
Points 	12

All ranged weapons the sharpshooter uses has short range +3" and Long range +5", allowing a longer attack range then any other trooper can make out of the gun.

Dominion Tactics

Opia has always and still uses defensive tactics on offense.  When invading a planet, generals throw all their forces into one area, to defend it until a fort can be set up (with proper SAPFT equipment), as well as some large gun, whether its a modified Bertha Artillery or a Large Chaos Gun.  These forts are usually set close enough to an enemy, so these super weapons can bombard the enemy.  This forces the enemy to attack a well fortified position.  After the enemy has been cleaned out in one area, the army fortifies another area and sets up another base, and on and on.

When defending a base without an army attactched to it, Sharpshooters are positioned on towers all around the inside of the shield ring, but a decent enough distance from the base.  Also, almost no regular universal trooper type is used for regular garrison; all Shielded Troopers, so an Opian commander has maximum mobility with the shielding up.

The Tri-Angalic Union

Type:  Nation
Government:  Federation
Ruler:  Overseer Humcum
Created and Submitted by:  J. G. Radford
Notes:  One of the massive nations that rule large chunks of the galaxy, the Tri-Angalic Union is actually three smaller nations that have united together to form a massive nation.  The Union doesn't see combat much with powerhouses such as the Democratic Empire or the Immortal Federation due to its isolated location in the galaxy.
History:

The Harakim Communities, the Neo-Trojan Protectorate, and the Mercenaries for a New Order, joined together into an alliance in 3028 and called themselves the Tri-Angalic Union.  This new unity created yet another super-power in the galactic war.  However, their position in the galaxy forces them to border small, crude, and weak nations, that are being fed out by other large nations on the other side of the galaxy.

Heroes

In some armies there are men who are honored for their services, either because of a stupendous deed, a high rank, lengthy service, or other forms of superority.  They are heroes, and because of these prerequisites they are better, tougher, and smarter then other troops.

Heroes are constructed with bits and pieces.  You must start out with a trooper type.  Then you add on extra charateristics to increase the troopers stats and abilities, as well as point cost.
 
Skill 	What is does 	Points
Extra Strength 	+1 to all Close Combat damage rolls 	1
Extra Speed 	+1" to movement 	1
Extra Endurance 	-1 to all damage 	1
Extra Attack 	One additional attack that turn 	3
Faster Mind 	+1 to initiative roll 	2
Leadership 	Counts as communication 	2
Martial Arts 	Close Combat Attack-Skill Required 3  Damage 2d6/Two Attacks 	2
Driver's Permit 	Allows hero to drive vehicles 	3
Marksman 	+1 to all ranged skill roll 	2
Extra Spell 	1 extra spell (psychics only) 	2+level of spell
Increase Spell Level 	Raises Psychic level roll by 1 	4
Weapon Specialist 	+1 to all rolls for specific weapon type 	3

All skills, except Leadership, Martial Arts, or Driver's Permit, are culimative, thus you can get the skill multiple times. For example, you could get speed twice, costs 2 points, and the hero can move +2". The max possible for purchasing one skill is 5.  The extra spell only works on a spell that is on a level that can be cast by the psychic.  You may make the spell roll before you add this stat if you wish.

Extra spell may allow the psychic access to spells above what he/she can abtain, but that doesn't mean he/she can use any of the spells in that higher level.  You must add in extra spell in order to use spells of a level abtained by the Increase Spell Level add-on.

Previous - Pirate Gangs  Next - Campaign Rules

