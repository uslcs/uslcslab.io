
Book 3: Division Level Combat


Previous - Vehicle and Base Weaponry    Next - Corperation Troopers

National Troopers

The starring roles in the Galactic War are the dozens of countries that want to combat each other for power, revenge, or tyranny.  Some are democratic, peaceful people, while others are greedy despots.  Yet they all share the same cause, tactics, and troopers.

This book gives you a choice:  Who will you play?  It all depends on experience, fighting style, and favoritism (some word I made up, don't worry).  The Nations are easier, more flexible, but don't gear towards the extreme sides, such as mostly really fast troops or mostly close combat orientated.  Here is the full list of the troopers you, as a nation, can utilize.  The pictures shown here are examples of what each mini-fig should look like (these are what I use).  They can be whatever you want.  I, however, encourage your troopers to be of a single color, to help easily identify your troops from theirs on the battlefield.

Standard Trooper

See Standard Trooper.

Scouts

Reconnaissance is one of the biggest ordeals of combat, next to the real thing itself.  Knowledge of your enemy can lead to decisive victories against sometimes overwhelming odds.  However, sometimes these recon officers, or scouts, can be attacked, which can be an entire battle scenario.  Other times their scanning and tracking equipment can be used for offensive purposes, which is their main cause for this game.
 
Name 	Scouts
Type 	Recon Officer/Fast trooper
Move 	7"
Skill 	1d6-1
Armor 	6
Points 	8

Scouts do have two special abilities.  One of them is to coordinate missile strikes and artillery barrages.  Whenever a scout gets within 12" from an enemy target, any friendly missile and/or artillery cannon within 60" may fire at that enemy unit using the long range requirement with a +2 bonus to Skill Roll (you would use Short Range if it is within short range).

Scouts are essential to firing anti-matter missiles.  When a scout gets within 10" of a target, you may choose to fire an anti-matter missile at the target.  This is done during the special phase.  It is an automatic hit but it takes one full turn until detonation, so get your guys the hell outta there!

Marines

Heavy infantry, yet slower, are tougher to gun down and can tear your ranks apart if you are not careful.  Marines, traditionally sea-faring soldiers, fight on board space stations hand to hand, board enemy vessels, orb drop to a location and capture it.  Usually, because of their great strength, they support the land borne troopers.
 
Name 	Marine
Type 	Heavy Space Trooper
Move 	4"
Skill 	1d6+1
Armor 	10
Points 	10

Marines have one distinct advantage, and that's their orbdropping experience.  They do not lose their movement phase after an orb drop;  instead they have a -1" penalty that next turn.  In addition to that, they do not have a penalty for air dropping, making them perfect paratroopers!

Medic

It is equally important to keep your soldiers alive as long as possible to deliver maximum damage.  Battlefield fatalities are great, usually ranging from 25% to a complete loss.  Medics help lower this by healing slightly harmed troops and by reviving dead soldiers.
 
Name 	Medic
Type 	Combat Doctor
Move 	5"
Skill 	1d6-1
Armor 	7
Points 	6

During the special phase, your Medics may attempt to heal a wounded soldier.  If on a skill roll the result is less then the current armor of the soldier, then the attempt to heal was successful.  Roll a 1d6 to see how much armor is healed back up, to the maximum Armor.

Squad Medics are not enough sometimes to heal a lot of troops quickly.  Medics may have specialized vehicles and bays inside bases to heal and revive troopers.  Medic vehicles cost an additional ten points and may not carry additional weapons.  Any attempt to heal a soldier inside a medic vehicle has a -2 to the required skill roll and +1 to the healing roll.

The Death Capacitor, which costs twenty points, is a way to revive fallen troops.  Placed inside vehicles, these makes dead troopers last indefinitely at zero armor, unable to do anything.  A medic may sustain a dead trooper for up to four turns on the field, but is unable to do anything else.  When a Medic vehicle arrives, it is assumed that the Medic carries the dead body inside and places it into the Capacitor.  If the vehicle does not make it in four turns (or if there's no medic available at the time of death), the soldier is lost forever.  Medics can drive Medic Vehicles.

Medic bays inside bases cost 25 points.  All attempts to heal a soldier is automatically successful and armor is boosted up to full.  If a dead soldier that has been stabilized been brought to the bay, he/she may be brought back to life.  One Medic can do this job during the special phase, but for every medic there, a +1 skill roll bonus is added to the Medic attempting the revival.  If the roll is a 5, the trooper is now at 1 armor and is ready to perform any actions.  If the roll is a failure, the trooper dies permanently and can no longer be revived.

Androids, described next, cannot be healed by Medics.  Medics may only make one attempt a turn.

Androids

Mini-figs have always performed actions well, but sometimes there is error.  That is where the mechanicised synthetic android, or just android, comes in.  Created to destroy the enemy, androids are faster, tougher, and smarter then humans, making them the ultimate combatant.
 
Name 	Android
Type 	Synthetic Soldier
Move 	6"
Skill 	1d8
Armor 	11
Points 	10

Androids, because of their strength, nullifies the first point of movement penalty when carrying a weapon.  For example, a power blaster would give no movement penalty, and a disrupter only gives off one movement penalty.

However, with all these advantages, androids, as well as other robots, suffer from one major problem;  the ability to perform with humans, not without.  In the case that an android(s) is left alone, which is if all humans in it's squad are killed, the android is unable to think of what to do next, so it must return to home base (or nearest squad or vehicle, if you don't have a base) in a straight line, without doing any special moves or attacking.  Once at the destination, the android may be assigned to another squad or base patrol.  Even with all these antagonisms, androids are a powerful potent tool in victory.

Psychics

Conventional warfare is one of the greatest things to ever hit the mini-figs of the galaxy.  However, since offense and defense was growing more and more, nothing new was happening.  For some odd reason, a mistake in some synthetic DNA caused an extremely large life force flow through the cell.  To enhance the soldiers in boot camp, they are given extra strength DNA.  One man got this mutated DNA, and became the galaxy's first psychic.  Women from all the nations were sent to retrieve the gene, and in a decade, all the nations were breeding psychics, the first unconditional way to wage ware.
 
Name 	Apprentice Psychic 	Master Psychic
Type 	Training Psychic 	Trained Psychic
Move 	4" 	5"
Skill 	1d4 	1d6
Armor 	5 	7
Points 	10 	18

Psychics have a special talent;  they have the ability to tap into the aura of space around them and mend it into battlefield weapons.  Psychics, however, cannot use the entire pool of spells.  They can only use a select number, and only in each level.  Apprentice Psychics can use 3 Level 1, 2 Level 2 spells, Level 3 spell and 1 level 4 spell.  Master psychics can use 3 Level 1 and 3 level 2 spells, 2 Level 3 and Level 4 spells, 1 Level 5 spell and 1 level 6 spell.  Before the battle, roll a 1d4 for each Apprentice Psychic you have and a 1d6 for each Master Psychic you have.  Whatever the number lands on is the maximum level that psychic can use for the remainder of the battle.  Count the number of spells avalible to the psychic above that level if (s)he did not get his/her maximum level, divide that number by 2, and use that to distribute among the other levels.  You can pick which spells you get in a level.
 
Level 	Name 	Range 	Effect
1 	Stand-Off 	10"* 	Psychic and target mini-fig may not do any actions until player owning psychic calls it off.
1 	Mind-Scan 	8"* 	Player must reveal target mini-figs armor.
1 	Psychic Sleet 	10" Radius 	All units moving through radius have half movement next turn.
1 	Psychic Extinguish 	Target Spell in Effect 	Cancels one spell in effect.
1 	Stim Up! 	6"* 	Target mini-fig moves double speed next turn.
2 	Field of Protection 	6" Radius 	-2 damage to any unit within radius, friend or foe.
2 	Telepathy 	Self 	Psychic has instant communication for five turns.
2 	Mental Heal 	Touch 	Target is healed 1d6 armor.
2 	Electrionic Recoupling 	12" 	Target weapon that is deactivated is reactivated.
3 	EMP Ray 	14" 	Disables either all weapons or all movement on one vehicle for one full turn; prevents attacks, moves, etc. (Flyers continue at same speed and direction)
3 	Mechanical Telekenesis 	6" 	Target vehicle, base, or android recovers 1d8 armor.
3 	Psychic Egg 	Unlimited* 	Target unit may not take or deliver damage and may not move.
3 	Psychic Forcefield 	5" 	Target is invulnerable from all spells for one full turn.
3 	Teleportation 	Self 	Psychic moves to any location he can look at.
4 	Amnesia 	8"* 	Target mini-fig forgets what to do, and loses a turn.
4 	Telekenesis 	10"* 	Moves any object a 2x4 block or a minifg and smaller for a distance of 6", even up.
4 	Psychic Cover 	4" Radius 	All units in radius are immune to all spells for two turns or until they leave the radius.
4 	Psionic Punch 	Unlimited* 	1d4 damage to target.
5 	Aura Shockwave 	Entire Battlefield 	All minifigs on ground must roll a 1d6; if it is a 5 or 6, (s)he falls over and loses a turn.
5 	Mind Control 	Touch 	Target trooper is brought under your control.
5 	Healing Field 	5" Radius 	All troopers inside field gain 1d6 armor.
6 	Duplicate 	Touch 	Makes exact replica of target allied trooper.  Weapons and equipment not duplicated.  If psychic, roll one less die roll for spells then the mini-fig who was copied has.
6 	Psychic Apocalypse 	Entire Battlefield 	All psychics take 1d6 damage roll.
6 	Gravity Well 	8" 	All objects move 6" towards this point minus the distance they are away from it, except bases.  Once an object reaches the point, it stops and loses a turn.
6 	Wind Storm 	Entire Battlefield 	All flyers move 5" in any direction you choose.
6 	Mind Wipe 	4"* 	Target Hero looses all hero stats and becomes a regular trooper.
7 	Sonic Scream 	Entire Battlefield 	All natural mini-figs (excludes androids, bases, and vehicles) take a 1d8 damage roll.
* - Line of Sight.  Does not go through windows or other clear objects.

Spells are cast during the Special Phase, and a spell may only be cast once by a single psychic in the same turn.  Spells wear off by the end of the turn unless stated and if their effects are not instantanous.

Commandos

While fighting is happening on the front lines, commandos sneak in behind to plant bombs inside the enemy base, or to kill a few important generals.  You always must be careful when in your base, because the best troopers in the galaxy are probably right behind you.
 
Name 	Commando
Type 	Covert Soldier
Move 	6"
Skill 	1d8
Armor 	9
Points 	20

Commandos do have one special ability, and that is out of communication movement.  If your commando is about to go on a mission, write down his/her mission.  The commando may go outside of communication and complete the objective.  Once the objective is done, the commando either:  Must stay in place until a squad or vehicle can pick him/her up, OR:  Find the closest communications source and head over towards it.

Commandos also have another important ability.  They can capture bases.  Base capture is described under Capturing Bases.

Spies

To counter measure commando strikes and to receive enemy intelligence, nations hire spies to complete these missions.  One of the most dangerous jobs, yet one of the most important non-combat military roles.
 
Name 	Spy
Type 	Espionage Worker
Move 	6"
Skill 	1d6
Armor 	6
Points 	15

Spies have two roles in the USLCS;  discover the enemy's weakness, and to counter the enemy's commandos.  If a commando comes within ten inches of one of your spies, the commando must reveal his/her missions.  If an enemy unit comes within 8" of one of your spies, the enemy player must reveal that unit's armor to you.

Since spies are supposed to blend in, they can be anything you want to be to throw off your opponite, so no pictures are displayed.  Spies may not carry weapons.

Diplomats

Diplomatic relations are often used to help defeat a common enemy, but usually dissolves when the two allies attack each other.  Unstable in the long run however, the short term time of the battlefield can be used for your advantage.  Nations use diplomats to discuss terms with one another during a large battle.
 
Name 	Diplomats
Type 	Peace Discusser
Move 	5"
Skill 	1d4
Armor 	5
Points 	7

If the Diplomat has reached the enemy base or leader, your opponite must decide if he/she wants to discuss with you.  If that is the case, then you may decide the following terms:

Cease Fire - Your two armies may not fire on each other;  however, stray explosions may kill off the other player accidentally.  Creates alliance.

Special Frequency - Diplomats are no longer needed to discuss with each other;  all of these available any time.

Mixed Squads - Your two forces may mix in with each other;  both forces considered 'friendly' by both players.

Strategy Discussion - If the two players want to discuss strategy, or just want to chat, you can.

Any of these may be called off if the Special Frequency is intact or the other player had brought a diplomat over to discuss the call off.  Cease Fire must be called off after Mixed Squads, not before.  If all the other players have been defeated, and the two or more allies are left, then they all win.

Diplomats cannot be shot at, but can die from explosions.  Diplomats cannot wield weapons.

Mechanic

Drivers have a high demand for their vehicles.  In between combat, they need to be in top condition;  the tires need to be shined, the interior cleaned, the weapons recharged.  Mechanics do this job.  On the battlefield, mechanics can repair any vehicle that has been damage or even destroyed.  Because of this, drivers and mechanics share a tight bond.
 
Name 	Mechanics
Type 	Vehicle Worker
Move 	5"
Skill 	1d6
Armor 	7
Points 	10

A Mechanic must have one tool to be able to repair.  A tool can be but is not limited to:  robotic arms, wrenches, drills, and hoses.  You and your opponites must decide what is or is not a tool.  A second tool adds a +1 bonus to the Mechanics repair roll, but costs 2 points.

If an android or vehicle is damaged, a mechanic may attempt to repair it if he/she is within 1" during the special phase.  Roll two skill rolls.  If the second roll is higher, then the mechanic succeeds in repairing and goes on with the process.  If not, the mechanic fails.  If the roll succeeds, roll a 1d10 and +1 if the Mechanic has an extra tool to see how much armor is repaired.

Mechanics rarely wander around the battlefield with his tools, finding any damaged vehicle he can find.  Mechanics may use their own vehicles as mobile repair units, vehicle towing, and/or spare parts storage.  Spare parts such as the chassis pieces are free, but anything like locomotives or weapons cost the amount they are worth.  A mechanic vehicle can carry 10 parts, at a -1" penalty per three chassis pieces or the regular penalty for other objects.

Heavy equipment cost 5 points each, but give the Mechanic a +1 skill roll bonus and a +3 repair roll bonus.  Each heavy equipment module inflicts a -1/2" penalty to the vehicle.  Mechanics may drive Mechanic vehicles.

Technology has advanced so much that a Mechanic may be able to build a destroyed vehicle from scratch.  He/she must have a vehicle with heavy equipment available for this attempt to work.    When a mechanic finds the remains of a vehicle, he/she must roll a 6 using their skill roll during the Special Phase (without the Heavy Equipment bonus) to make the attempt a success.  If it fails, gameplay continues.  If the roll succeeds, you have thirty seconds to rebuild the vehicle using the remains on the field and using the spare parts inside a vehicle, taking them out as needed.  The vehicle is considered complete when the thirty seconds are up or if you are done, so you can no longer rebuild the vehicle.

Mechanic bays in bases cost fifty points, and no heavy equipment are needed (you can have some to look flashy).  All repair and reconstruct rolls are automatically successful.  Repair rolls have a +5 armor bonus, and vehicles take two minutes to reconstruct.  If it is not completed by then, reconstruction may continue next turn.  Extra pieces may be stored inside the bay.

Mechanic may not carry weapons, and does not need to belong to a squad, but does, however, require communication.  Mechanics may only make one attempt a turn.

Engineers

Maintenance of a base is very important.  A base is the soldier's homestead, and it needs to be kept in top condition.  The military hires engineers from the civilian division to build bases, in and out of battles, and to maintain them so that no enemy may breach the walls.
 
Name 	Engineer
Type 	Structure Specialist
Move 	5"
Skill 	1d6
Armor 	7
Points 	10

Engineers, during the Special Phase, may attempt to repair a damaged wall.  He/she must roll a 4 or higher on a skill roll.  If it succeeds, a 1d12 roll is needed to decide how much armor is repaired.  Since engineers are highly trained scholars, unlike their 1.2 GPA mechanic counterparts, engineers do not need tools.

Engineers are also used to build field bases.  Out of battle, material is shipped from the asteroid mines, where the engineers put the pieces into place carefully.  In battle, however, engineers can quickly make synthetic material and stack blocks on top of each other to form crude, yet efficient walls.  During the special phase, an Engineer may create one 2x4 or 2x2 block out of nothing (it is curious if that magical ability is due to their 4.72 GPAs all through school).  An engineer may not repair and make a block in the same turn.  A single block carried by a mini-fig inflicts a -2" penalty (-1" for androids).  Soldiers kept in reserve, off shift mechanics, and other engineers are used to carry the blocks to where they are needed and drop them into place.  This is done in the Special Phase.

Engineers can not wield weapons.

Technician

With the hard working mechanics and the suave engineers, battlefield maintenance is almost perfect, assuming that both mini-figs survive the course of battle.  However, weapons need to tuned up also, so the military call upon computer nerds to handle the systematic.
 

Vehicle Crewmen

See Vehicle Crewmen.

Previous - Vehicle and Base Weaponry    Next - Corperation Troopers
Name 
