---
title: "Basic Trooper"
---

Book 1: Platoon Level Combat

# Standard Trooper

Armies are mostly made up of the simple 'Standard Trooper', who, like it's name says, are standard military personnel.  They are the common man or women to graduate from boot camp.  There they learned how to fight, how to accept orders, and how to live under constant alert.  There they are also genetically altered to endure the tyranny of the battlefield.
 
| Name |	Standard Trooper |
|------|-------------------------|
| Type |	All Purpose Infantry |
| Move |	5" |
| Skill |	1d6 |
| Armor |	8 |
| Points |	5 |

Standard Troopers can move up to 5" a turn.  They have a 1d6 skill roll in using weapons and equipment.  Their armor is 8, allowing them to suffer up to 8 damage before they die.  And also, they cost a moderate 5 points each, not including weapons cost.  This can allow for some serious numbers!

Unlike vehicles, troopers can do many things, due to their small size and small cost.  They can perform raids inside base walls.  They can drop in from orbit behind enemy lines.  They can swarm pesky little snipers.  They can be of very good use, in a small 100 point skirmish or in a huge 4000 point battle.

# Infantry Concepts

There are several concepts that infantry must abide by and certain concepts that they may use.  Here they are:

Squads:  Infantry are organized into squads.  These squads can be anywhere from three people to 20, or even further, if you really want that.  Squad members care for each other, protecting each other from weapons fire, and make sure that every one else lives.  Two squads can merge together at any time, and a squad with six or more troops may split.  However, squad members must be within 2" of another squad member, and the squad must be in communication to allow regular control.  No communication cancels out the movement phase.

Orbtroopers and Airtroopers:  Now with the ability to drop troopers from ships with orbit, your forces may deploy at any time during the battle anywhere.  Orbtroopers cost an additional 10 points, for their anti-shock boots and heat shielding.  They cannot be present at the beginning of the battle.  During the Special Phase, you may declare one squad of orbtroopers to begin their descent.  Designate exactly where they are going to land.  At the end of the next Special Phase, place your troopers there.  They may not move that next movement phase, but they may still attack.

Airtroopers cost 5 points for their Anti-shock Boots.  They do not take any damage from falls, thus they can drop from flyers.  When dropping from flyers, it is declared during the movement phase, and they land at the end of the special phase, with a -2" penalty for moving that next movement phase.  However, they land directly below the flyer at the time the troopers dropped, not where the flyer is currently.

Any trooper that falls without anti-shock boots suffers 1d4 damage per 2", rounded up (so a Standard Trooper falling 3" suffers 2d4 damage).

Communication:  Like above, squads need communication to operate correctly.  If they do not have communication, either from a communication device or from a Hero with leadership, you can have the following effects:  The squad may not move at all;  The squad may move every other turn;  You and another player alternate movement every turn.  Walkie-Talkies cost two points.

Movement and Firing Arcs:  Troopers may move forward at full speed, or backwards at half speed.  He/she may also land on his/her stomach during the special phase, halving movement but adds cover potential.  Troopers can generally shoot anything in front of them.  However, if they are shooting something that is to their side, then they get a -1 penalty on their skill roll for shooting it.

# Infantry Weapons

Infantry weapons are the most common types of weapon on the battlefield, unless you are a tank freak and care only about vehicles.  In version 1.x, these were divided up into three tables:  Ranged Weapons, Close Combat Weapons, and Explosives.  Now they are further divided up, to help you know what each particular weapon is meant for.  The explanation of each table is as follows:

        Short Range - The maximum short range distance (for ranged weapons only)
        Long Range - The maximum long range distance (for ranged weapons only)
        SR Skill - The minimum requirement for hitting at short range (excludes explosives)
        LR Skill - The minimum requirement for hitting at long range (for ranged weapons only)
        Damage - Damage weapon deals
        Move Penalty - Number of inches subtracted off the mini-fig's movement
        Points - How many point weapon uses up
        Two-Handed - Weapon may use both hands of the mini-fig

## Pistols
 
| Name 	| Short Range |	Long Range |	SR Skill |	LR Skill |	Damage |	Move Penalty |	Points |	Two-Handed |
|-------|-------------|------------|-------------|---------------|-------------|---------------------|---------|-------------------|
| Blaster Pistol |	5" |	10" |	3 |	6 |	1d3 |	-- |	3 |	-- |
| Heavy Blaster |	6" |	12" |	3 |	6 |	1d4 |	-- |	4 |	-- |
| Photon Blaster |	6" |	14" |	3 |	6 |	1d6 |	-- |	5 |	-- |

![Pistols](/img/pistols.jpg)

Pistols are designed to kill infantry.  Useful for moping up scattered squads, for they would probably be retreating, and pistols impose no movement penalty, so you'll have no problem chasing them down.  In addition, doubling up on pistols can cause havoc to squads of troopers that outnumber you.

## Rifles
 
| Name | Short Range |	Long Range |	SR Skill |	LR Skill |	Damage | 	Move Penalty |	Points |	Two-Handed |
|------|-------------|-------------|-------------|---------------|-------------|---------------------|---------|-------------------|
| Sniper Rifle |	14" |	30" |	4 |	6 |	1d6+2 |	-- |	8 |	yes |
| Auto Photon Rifle |	8" |	16" |	2 |	5 |	1d8 |	-- |	7 |	yes |
| Power Blaster |	8" |	20" |	4 |	6 |	2d6 |	-1" |	9 |	yes |
| Plasma Flamer |	3" |	8" |	2 |	4 |	1d6 |	-- |	8 |	-- |

![Rifles](/img/rifles.jpg)

Rifles are commonplace among infantrymen.  Their power and range makes them incredibly useful on the field.  Even though they are used on infantry, rifles can damage small vehicles.  A 6 man squad armed each with a rifle can pretty much destroy any vehicle smaller then 30 dots in one turn.

A Plasma Flamer delivers Fire Damage.  That is introduced in the next book, but if you want to use it in this book, here is a simplified version:  If it hits the target, roll the damage roll.  That next Special Phase, flip a coin.  If it lands in your favor, the target takes another 1d6 damage.  Do this until you fail a coin flip or the target dies or is destroyed.

## Destruction Guns
 
| Name | Short Range |	Long Range |	SR Skill |	LR Skill |	Damage | 	Move Penalty |	Points |	Two-Handed |
|------|-------------|-------------|-------------|---------------|-------------|---------------------|---------|-------------------|
| Disrupter |	10" |	20" |	4 |	6 |	2d8+3 |	-2" |	10 |	yes |
| Atomic Chain Gun |	8" |	18" |	3 |	5 |	3d6 |	-2" |	12  |	yes |

![Destruction guns](/img/dad.jpg)

If you want infantry to be of use verses the battle tank, you'll need these.  Several nations saw troopers pounding tanks with rifles and pistols, but they usually got ran over.  So they developed Destruction Guns, also known as DAD Guns (Death and Destruction).  These massive weapons slow troopers down quite a bit, but they have good range, and do massive amounts of damage.

Disrupter is made of two gun pieces of any previous guns.  An Atomic Chain Gun is two Auto Photon Blaster pieces hooked together by a cone or something like that.  A Grenade Launcher is three of any gun pieces stuck together.

## Infantry Explosives
 
| Name |        Damage |	Move Penalty |	Points |	Two-Handed |
|------|---------------|---------------------|---------|-------------------|
| Mini-Bomb |	1d10+3 |	-1" |	4 |	-- |
| Regular Bomb | 	2d10 |	-2" |	6 |	-- |
| Large Bomb |	2d10+3 	-3" |	8 |	-- |
| Mega Bomb |	3d10+2 	-4" |	10 |	yes |

![Grenades](/img/grenades.jpg)

Infantry Explosives are the weapons to counter the missiles and bombs used against them by enemy vehicles.  They compose of all the grenades and timed mines used in the Galactic War.  Using this book only, grenades are going to be pretty useless.  Here is how they work.

Grenades are hurled at the enemy line.  Unlike other weapons, where you target a specific unit, grenades are thrown in a specific direction.  To see how far they go, take 4" (or less, if you want to), roll the skill roll roll of the trooper throwing the grenade, add the two numbers together, and take the move penalty and subtract that from the other number to see how many inches it is thrown.  Grenades are thrown in the Special Phase, and detonate in an explosion effect at the beginning of the attack phase.  Mini-Bombs and Regular Bombs are common grenades;  Large Bombs are sometimes used but don't go very far; whoever throws a Mega Bomb would most likely get hit from the explosion, so don't use those too often.

Timed Mines are generally Large and Mega bombs, but sometimes the smaller two are used in the case of booby traps in bases.  A Timed Mine is planted where the trooper carrying it is at the Special Phase.  Write down on a piece of paper how many turns until detonation.  Only Technicians and Commandos may disarm timed mines, see National Troopers.  After the amount of turns pass by, at the beginning of the Special Phase, the bomb goes BOOM, and deals explosive damage to it's surroundings.

## Energy Blades
 
| Name |	Skill Required |	Damage |	Movement Penalty |	Points |	Two-Handed |
|------|-----------------------|---------------|-------------------------|-------------|-------------------|
| Power Knife |	2 |	1d6 |	-- |	2 |	-- |
| Laser Sword |	3 |	1d8+1 |	-- |	3 |	yes |
| Plasma Sword |	3 |	2d6+2 |	-- |	4 |	yes |
| Chain Saw |	5 |	3d6 |	-1" |	4 |	yes |

![Swords](/img/swords.jpg)

Energy blades are the most common close quarter weapon type.  Each is an actual blade, but energy discharges makes the blade more durable and increases damage (the Power Knife is not energy, but it is in here, anyway).  They are easy to use, cheap, and deliver hectic damage.  The only one you'll have trouble using is the Chain Saw, but it can cause messy damage.

If a trooper is meant for brawling, he should be equipped with an energy blade.  Unless the trooper is meant to attack with close combat weapons at long range, (s)he should strike have an energy blade on hand.

## Spears
 
| Name 	Skill Required 	Damage 	Movement Penalty 	Points 	Two-Handed |
| Super Javelin 	3 	1d6+2 	-- 	3 	--  |
| Thrust Spear 	4 	1d8+1 	-- 	3 	yes (no if throwing) |
| Electric Lance 	-- 	2d6 	-1" 	4 	-- |

![Spears](/img/spears.jpg)

Spears are deadly weapons.  Even though blades are the best in a giant brawl, spears are powerful in long ranged throws.  Spears may be thrown just like grenades, starting with 4", rolling the skill roll and then subtracting the move penalty.  However, since the spear is thrown at enemy lines, and not over, it is not an arching weapon.  If there is a target between you and where your spear landed, you may choose that target to be hit.  Thrown spears deliver double damage, and can be retrieved where ever it landed.  The Electric Lance cannot be used in regular close combat.

## Blunt Weapons
 
| Name |	Skill Required |	Damage |	Movement Penalty |	Points  | 	Two-Handed |
|------|-----------------------|---------------|-------------------------|--------------|------------------|
| Fist |	2 |	1d3 |	-- |	-- |	--  |
| Gun Butt |	3 |	1d4 |	-- |	-- |	-- | 

Blunt weapons are not weapons themselves, but actually parts of other weapons.  A fist is a trooper using his hands to attack his enemies.  A Gun Butt is hitting your enemy with the butt of your gun.  Use it if you have no other option.  Two Gun Butts may be used if you have two guns; two fists may be used if you have two empty hands.

## Weapon Enhancers and Trooper Equipment
 
| Name |	Effects |	Points |
|------|----------------|--------------|
| Aerodynamic Skies | 	-1 movement modifier; Increases movement by +2 going downhill every turn; -2 movement while moving over flat ground or uphill until original movement 	 | 5 |
| Fire Extinguisher |	Range of 6"; Needs 2 on a Skill Roll; Decreases Fire Damage by 1d6 |	4 |
| Backpack |	Carries weapon or equipment at half movement penalty; equipment or weapons carried this way uses no hands |	5 |
| Thrust Pack |	Double Movement; flies | 	6 |
| Jump Jets |	Leaps 6" into air every time trooper moves |	6 |
| Walkie Talkie |	Allows Communication |	2 |
| Anti-Shock Boots |	Prevents damage from falling |	5 |
| Heatshielding Coat |	Allows Entry into atmosphere from orbit |	5 |

![Equipment](/img/equipment.jpg)

These equipment are to help any trooper survive on the battlefield.  Even though they will weigh you down in points, troopers equipped with these will surely deliver more damage and escape easier when in trouble.  The Thrust Pack, Head Set, Anti-Shock Boots, and Heatshielding Coat are not in the picture:  Head set is painted onto the mini-fig's head, and thrust pack is unavailable in the MLCad directory.  The latter two are just imaginary.

Aerodynamic Skies hinders regular movement, but going downhill the trooper will get a +2" bonus to their movement every turn.  A trooper who normally moves 4" with the skies would move 6" on the first turn downhill, 8" the next turn, 10" the turn after that, and so on.  However, they lose their movement on regular ground as fast as they gain it.  Once a trooper with skies stops going downhill, they lose -2" every turn.  A trooper that just went 10" downhill would go 8" on the next turn, 6" on the second turn, and back down to his original movement of 4" on the final turn.  This can get a trooper out of a situation quickly.

Thrust Packs allow more efficient, stable movement.  Their hefty point cost makes them uncommon on the battlefield, but it is more reliable then skies.  It doubles a trooper's original movement, before movement penalties are placed.  A Standard Trooper wielding a Power Blaster would move 9" a turn, with his movement of 5" doubled and then the move penalty added on later.  Also, the flying ability of the Thrust Pack is used when crossing crevices.  If another location at the same altitude as the trooper is separated by a gorge or something, as long as it is within the trooper's maximum movement, he/she may cut across the air to get to it.

Jump Jets are jets that thrust a soldier into the air.  This allows the trooper to freely move over walls and other terrain obstacles.  Any object less then 6" tall can be crossed by a mini-fig with jumpjets.  The mini-fig may also land on such an object.  This allows the trooper to storm encampments without any trouble of climbing the walls or waiting for vehicle weapons to bash the wall down.

Communication was described above.

Anti-Shock boots and Heat Coats are described above.

[Previous - Gameplay](/src/gameplay.md)   [Next - Advanced Gameplay](/src/advanced_gameplay.md)

