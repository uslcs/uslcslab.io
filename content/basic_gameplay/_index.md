---
title: "Book I: Basic Gameplay"
---


* Book I Basic Gameplay
    * [Basic Gameplay](./GAMEPLAY)
    * [Basic Trooper](./basic_trooper)


# Introduction

Welcome to Space. You and your ship are out in the cosmos. You could be a near-future surveyor, prospecting asteroids and harvesting ice in the solar system. You could be the bridge crew of the flagship of your federation. You could be Emperox whos cultured throne is in a space station of grandeur. Most importantly, you will be a character in a story.

USLCS offers three ways to play:

* Guided: One player takes on the role of the Game Master. The other player(s) take on the role of their characters in the story. The Game Master works with the players to make the universe come alive. They also represent the other characters and creatures that they players encounter.
* Cooperative: You and friends play together to tell the story of your Minifigures. A guide is not required. Using the USLCS game system (inspired by Starforge, Traveller, and Stars Without Number) will help build the galaxy and the nature of those who live in it.
* Solo: Like cooperative play, no Game Master is needed. You will tell the story of a character.

This rulebook will be written for the perspective of the solo player, but the rules and systems are the same for co-op and guided play.

# Using This Rulebook

You do not need to read this book cover to cover in order to play. Here is a summary for each chapter.

Chapter 1: The Basics: You are reading it. This is an introduction to USLCS and the fundamentals of basic play.

Chapter 2: Launching your Campaign: This is a series of exercises to initialize your story. You are always welcome to develop (or borrow or steal) your setting and characters on your own, but these exercises can help you fill in the gaps, or develop a whole universe from scratch.

Chapter 3: Gameplay in Depth: Extended game mechanics for your adventures in the stars. After you feel comfortable with the basic game loop, look here for more specific and advanced mechanics.

Chapter 4: Foes and Encounters: A table of foes, foils, and encounters for your characters. Rules on managing other characters are here as well.

Chapter 5: Oracles: These tables are the mechaniations of playing USLCS without a Game Master. This chapter is not required for imaginative play, but can be a source of inspiration when determining the outcome of an action, part of the setting, or other narrative events.

# What You Need To Play

* Two ten sided dice (2d10). These are challenge dice and oracle dice
* One six sided dice (d6). This is the action dice.
* Character sheet
* Starship sheet
* A notebook to record your adventures
* Time. Play can be as short from a few minutes to a few hours.

# Tone 

Unlike other Powered by the Apocalypse Games, it will be up to you, the players, to define the tone of the game. The tone will be one of the most important decisions you make, because all other decisions depend on the tone. Think of the diversity of space speculative fiction. Which Star Wars movie or series, or Star Trek series inspire you? The Expanse (early, mid, or late series)? Firefly? Alien? Farscape? Dune? Guardians of the Galaxy? Battlestar Galactica? Muppets in Space? Write down and agree on the tone. Think on how scarce the resources are, quest driven adventures or political intrigue. Hard Science to magical fantasy. Humans only or diverse sentient species. You can even use other RPGs such as Kingdom or Microscope to begin defining the tone of your world.

# Setting

Chose the scale. Are your people limited to the star system they are in, or can they FTL across the stars? Is this our humans in the future, or the 1990's Space LEGO universe?

# Mechanics and the Fiction

USLCS is a game. It uses mechanics, such as rolling dice, building, and Minifig movement rules. You will often want to make decisions on a desired mechanical outcome, such as choosing an action to get a bonus on a die role.

USLCS is also about the fiction. This is the narrative of the characters, settings, and events. You will be envisioning actions and events in a way that will be consistent with the fictional reality within the story.

## Mechanics and Fiction Interacting

You start by picturing the situation. What is happening? What are you
trying to do? How are you doing it? If you are playing solo, imagine it. If
you are playing co-op, talk it out. If you are the guide, set the scene for
your players and work with them to add or clarify details.
Then, if a protagonist faces uncertainty or danger, the mechanics help
resolve the situation. You roll dice, check the result, and translate that
outcome back to the fiction. How does the situation change? What
happens next? In this way, the fiction bookends the mechanics.

You’ll learn how the mechanics work in this chapter, but keep in mind this
balance between those mechanics and the fiction. Without mechanics,
your story lacks choices, consequences and surprises. Without fiction, the
game is an exercise in rolling dice. USLCS relies on both the
mechanics and the fiction, but leads and follows with the fiction.
