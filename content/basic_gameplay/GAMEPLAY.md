---
title: "Gameplay"
weight: 9
---

# Gameplay

Before we begin this manual, we must explain to you what the core rules are for this game.  All of your army units compose of several if not all of these statistics.

Points:  Points is used to determine if the battle will be even.  Everything in the USLCS has a point value.  At the beginning of every battle, decide a point limit.  Your army must be the equivalent of the point value or less.  A 600 point limit is imposed on the game, and you can use up to 600 points.  You decide to use 598 points.  That is fine.  601 points, however, is not accepted.

Move:  Movement is done in inches.  If you are accustomed to metric measurement, you can somehow make a small transition, like 2 centimeters equal one inch of movement for gaming purposes.  A Standard Trooper would move ten centimeters in that case.  Movement is the maximum distance a unit may travel in one turn.  Anything except bases and other stationary objects like trees and boulders can move.

Skill:  Skill is the measurement on how well a mini-fig can use a weapon or perform a special action, like repairing a vehicle.  Only troopers have skill rolls.  Skill rolls are measured in dice and sometimes have modifiers.

Armor:  Every single unit in the USLCS has armor.  Trees have it, mini-figs have it, tanks have it, anything has it.  Armor is how much punishment the unit can take before blowing up (or dying, in the case of mini-figs).  The higher the armor, the more damage a unit can take before being destroyed.

## Pre-Game

In the USLCS, you don't just jump into the battlefield and to trash your opponite's army.  You must set up the battlefield to trash your opponite on.  This is a step-by-step instruction on what to do before gaming.

Step 1:  You and all your opponites must decide where to play the battle.  Card Tables, Coffee Tables, or anything along those lines work.  Anything larger then 1500 points or so would need a floor.

Step 2:  All players will then set up terrain on the playing surface.  Terrain types are described below.  If you really want a fun battle, have it in a gorge or something along those lines, where it would be difficult to attack your enemy.

![Table](../img/surface.jpg) ![Terrain](../img/terrain.jpg)

Step 3:  Among the other players, decide a boundary line going down the playing surface.  This boundary marks where your forces are going to be deployed.  This wouldn't really affect the outcome of the battle, but it could decide how the beginning of the game goes.

Step 4:  Place any buildings and bases on your side of the boundary.  After that place your vehicles and mini-figs down on the playing surface on your side of the boundary.  Behind a clump of trees, on the top of fallen logs, in the middle of a creek, anywhere.  If you resent placing your forces all at once, roll an Initiative roll (described below) to see who places the first unit, and so on.  This is the last step you need to do before starting.
Terrain:  There are two types of terrain:  Natural and Plasticmade.  Natural terrain, such as trees, hills, and rivers, are placed where all the players decide.  Plasticmade terrain, however, is put in by a single player only.  These include landmines and tank stoppers.  Plasticmade terrain can only be place on your side of the boundary line.  Any natural terrain that is destroyable, such as trees, has 8 armor.  Exceptions to this rule may apply.

Landmines cost 5 points each.  Non of your units can trigger an explosion.  However, any other unit that gets within 1" takes a 2d10+3 damage roll.  Even though your opponites know where they are, a whole mine field will prevent his vehicles from reaching your army any time soon.  Tank Stoppers cost 5 points for every inch of it you lay.  They deal a 1d6-1 damage roll to mini-figs climbing over it, and vehicles take a 1d12+4 for colliding with it.  Mines cannot be destroyed until they detonate, and each inch of tank stoppers has 10 armor.

## The Game

![Setup](../img/setup.jpg)

The game itself is divided up among turns.  Each turn composes of four phases.  Each of the phases are described below.  Each phase is divided into rounds.  The game begins with the first phase, and after everybody has done what they wanted to do in that phase, gameplay goes on to the next phase.  After the last phase, the turn ends and a new one begins, starting with the first phase.  Gameplay continues until one player is left alive.  The other players have either surrendered, been killed off, or retreated.

Phase 1 - Initiative:  Every player rolls a 1d6 die.  This determines which player has a greater response time then the others to the previous turn, thus that would tell who would do actions first in each phase.  The second highest goes second, and so on.  Ties are solved by rerolls.

Phase 2 - Movement:  The player who won the initiative roll moves one mini-fig or vehicle up to it's maximum distance.  The next player does the same, and so on.  After the last player moves something, the first player moves something else, and so on.  You may only move something once a turn.

Phase 3 - Special:  Also called the Miscellaneous Phase.  Gameflow works just like the movement phase.  Actions such as casting a spell, picking up a weapon, or healing a trooper occurs during this phase.

Phase 4 - Combat:  This is the phase where all attacks (except bomb drops and spells) are made.  The first player chooses one vehicle, squad, or base to fire it's weapons.  Weapons must target a specific object, such as a mini-fig, building, tree, or a laser cannon.  The target must be in range for the weapon to fire, and the necessary skill rolls are made to see if the target is hit.  If it is, the target takes damage equal to the damage roll of the weapon.  The total damage is the amount deducted from the target's armor.  If it goes to or below zero, it is destroyed.  Gameflow functions like the other two phases.

## Combat

There are two types of combat in the USLCS:  Ranged and Close.  Ranged Combat takes place over a distance, sometimes as close as several inches to as far away as five feet.  Close Combat takes place right at each other's throat.  This section will explain the basics on how each function.

![Ranged Combat](../img/ranged.jpg)

Ranged combat involves weapons that fire energy blasts, projectiles, or something in between.  When you are attacking, make sure the target is within range.  There are two range levels for ranged weapons:  Short and Long Range.  Decide wither the target is in short range or long range.  Then roll the mini-figs skill roll.  If it meets or exceeds the requirement for that range, the blast hits.  Roll the necessary damage roll.  For example, a Power Blaster delivers 2d6+2 damage, in which you would roll two six-sided dice, add the results together, and add two to get how much damage it delivers.

![Close Combat](../img/close.jpg)

Close Combat covers very little distance.  It composes of two or more troopers whacking each other with fists, energy swords, chain saws, and stuff like that.  The maximum distance for close combat is 1", but if the weapon extends further it can hit the trooper farther away.  With all these photon based weapons and long ranged explosives, the need for close combat weaponry has died down the past couple hundred years, but they are cheap and deliver good amounts of damage.  Like ranged combat, you roll the skill roll to see if you met the requirement, and then roll the damage roll to see how much damage you delivered.

[Next - Basic Trooper Needs](src/basic_trooper.md)
