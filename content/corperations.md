

Book 3: Division Level Combat

Previous - National Troopers    Next - Civilians

Corporation Troopers

Keeping the commerce of such desperate times are the Corporations.  These businesses travel from country to country supplying business such as movie making, bank accounts, spoon manufacturing.  One nations could supply a company with a factory for, say, forks, and another country wants to buy them, so trade routes are established.  The nation pays, and since the company has to pay rent, they use some of their profit as taxes, so essentially, everybody is trading with each other without knowing it.

Recently, nations discovered this and began attacking corporation supply networks and giving the excuse of "restricted territory - all intruders will be attacked" crap.  These corporations, using their large profits, began arming their supply ships and Customs officers, forming small yet tough armies.  Unlike nations battling each other on the border, they are facing an enemy from within...the once innocent capital businesses.

Corporations may use Diplomats, Spies, Mechanics, Engineers, Technicians, Medics, Androids, and Vehicle Crewmen from the National Troopers list.  However, they do have their own special list:

Security Guards

Titled this because of their former job as headquarters police and customs officers, Security Guards are a fierce bunch.  Because of their weaknesses compared to the Standard Trooper of the nations, Corporations mainly use vehicles, with some of these guys as escorts.  Security Guards, however, have a +2 bonus to Close Combat damage, making them worse up close.
 
Name 	Security Guard
Type 	Escort/Patrol Officer
Move 	5"
Skill 	1d6-1
Armor 	3
Points 	3

Security Guards may only use pistols, explosives, and close combat weapons.

Commercial Psychic

The weakest form of psychic known in the galaxy, commercial psychics are not bred to destroy, but quite the opposite:  to create profits.  Their purpose is to make sure no one cheats in their deals, but almost all the time the psychics bribe their target mentally.
 
Name 	Commercial Psychic
Type 	Business Psychic
Move 	4"
Skill 	1d4
Armor 	3
Points 	10

Since Commercial Psychics are weaker then even Apprentices, they may only have one spell and may cast it every other turn.  However, their main advantage is the ability to bribe enemy troops, to strengthen the corporation's army.  The psychic must be in one inch from the victim you wish to transfer.  Roll a 1d10 during the Special Phase.  If it is a 9 or 10, that trooper is automatically brought under your control.  This attempt may only happen once a turn.

Artificial Intelligence

The newest in the corporation's labs, A.I.s have been a dream for many generations.  First came the revolutionary android.  Now, they are used to operate bases and vehicles faster then anything before.  Because they are faster, A.I.s may shoot two guns a turn, even facing separate directions.
 
Name 	A.I.
Type 	Enhanced Operations
Move 	--
Skill 	1d8
Armor 	--
Points 	10

A.I.s may act as the Main Computer, if you wish, for a base.  Unlike Androids, A.I.s do not have to be next to human mini-figs, for it has an advanced algorithm far beyond what the Android has currently equipped.  A signal weapons blast destroys the A.I., and Technicians may fix it like a weapon, see National Troopers.

Previous - National Troopers    Next - Civilians
