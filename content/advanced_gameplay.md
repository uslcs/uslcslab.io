---
title: "Advanced Gameplay"
---

Book 2: Battalion Level Combat



# Advanced Gameplay

Welcome to Book 2.  I see that thus far your demands have not yet been met.  Gameplay was rather simple, and pretty much the game would get monotonous very quickly.  Now, there are many combat modifiers, movement modifiers, fire damage, near hits, and explosions, so, have fun!

# Ranged Combat

Ranged Combat hasn't really changed much, but there are now several modifiers to hitting your enemy.  Depending on the situation, the target can be easier to hit or harder to hit.

If the shooting mini-fig/vehicle has been standing still for one full turn (has not moved this turn) or the turret/hinge turned no more then 45 degrees, the skill roll gets a +2 modifier, due to the time aiming at his/her prey.  However, if the target is 1/3 hidden, there is a -1 modifier to his/her skill roll.  A target 2/3 hidden allows a -2 penalty to the skill roll.  A specific target all the way hidden can only be shot at by an arcing weapon at a -4 modifier.  Arcing weapons include grenades, grenade launchers, artillery, and missiles.  Targets that has not moved allow the shooter to get a +1 bonus to his/her skill rolls.

Size of the object can also go into consideration.  If the object is smaller then a mini-fig or a 2x4 brick, there is a -1 penalty to skill rolls.  An object bigger then 40 dots, however, will be easier to hit, and shots targeting such an object will get a +1 bonus to the skill roll involved.

If the object is moving fast, it is obviously going to be hard to take good shots at it.  For every 6 inches the object moved the turn you are firing, a -1 modifier is given to the skill roll targeting it.  This makes flyers really hard to hit, because most of the time they are going 15" and faster, which is a -2, maybe a -3 modifier to shots targeting them.

Point Blank Range are always automatic hits.  For trooper weapons this is at a range of 1";  vehicle weapons have a range of 2".  If you think that more calculations must get involved, here's this;  if the target is larger then the 45 degree firing arc of the weapon, then it is an automatic hit.

# Close Combat

Close Combat is now more brutal.  Instead of one mini-fig swinging his blade or club, and another one doing the same, there are 'brawls' that mini-figs may get into.  This only occurs when one mini-fig declares a close combat attack against another mini-fig.

The trooper makes the appropriate skill roll and damage rolls.  Right after that takes place, the trooper he is attacking may make a counter-attack.  Counter-Attacks do not count as their attack for that turn.  Counter-Attack resolution is done the same way as normal close combat attacks.  When it is the other player's turn to attack, he may wish for that particular trooper who had counter-attacked an attack to attack the trooper who made an attack on his trooper, and that trooper may make a counter-attack, if he is still alive.  This can make huge brawls fun and entertaining.

Gang-up tactics may be used to resolve problems from a particular trooper.  Up to four troopers may engage in a brawl with one enemy trooper.  That trooper may make counter-attacks to each attack made by the troopers ganging up on him/her.  He/she has a -2 skill roll modifier counter-attacking the second attacker, -4 for the third trooper, and -6 for the fourth trooper.  By that time the ganged-up trooper would probably be some body fluids on the nearby grounds, so don't wish your guy would last that long.

When a brawl is in effect, both (or all) troopers may not make any moves.  If one trooper wishes to get out of the brawl, he may not make any moves, attacks, or counter-attacks that turn, but if he survives the combat phase, he/she is now out of the brawl, and is free to do regular movement.

# Terrain Effects

Sure, terrain can be used as cover or places to hide, but really, they are in here for movement modifiers.  Some terrain is easier to trek over then others, such as plains or basin.  But some is difficult, such as cliff sides and rocky ground.

A mini-fig climbing uphill from a 10 degree to a 45 degree arc has a -1" modifier.  A vehicle trekking this gets -2".  Any hill steeper, up to a 60 degree arc, is half movement penalty.  Anything steeper is considered climbing, which is allowed only for mini-figs.  Anything 2 bricks tall or less has no penalty.  Any wall up to 5 bricks tall is at a -1" penalty for climbing up.  A barrier up to 8 bricks tall is a -2" penalty for climbing upwards.  Going the opposite direction, downhill, causes all penalties to turn to bonuses.

Vehicles can move easily over bumps.  A treaded vehicle moving over a one brick tall wall is at a -2" modifier, and hovercraft and walkers get none.  A treaded vehicle going over anything two bricks tall is at half movement.  Walkers have a -2" penalty.  Treaded vehicles attempting to go over a barrier three bricks tall or higher collides with it.  Walkers go over bricks stacked up 3 tall at half penalty, and anything higher is a collision.  Collisions are described below.

Water is another story.  Moving into and out of water is at half speed.  Traveling in water doubles all movement penalties.  A Standard Trooper carrying a Disrupter would move -1.5", in which it would not go anywhere.  She'd have to ditch her weapon, or drop to the bottom.  Vehicles won't have much luck ditching her weapons, but maybe the crew can force the passengers to bail out.  Movement at the bottom is halved, but movement penalties are not doubled, making it easier.  If it is a deep pool, stop wishing you'd get the unit to the other side.

Bumpy terrain imposes a half movement modifier.  Flyers and gravshipsare not affected by any terrain modifiers at all.  Why?  Call your doctor to find out.

# Explosions

The possibility of battlefield combustion is very common.  The use of long ranged explosives have been at hand for hundreds of years, before space faring was a reality.  Explosions, though inaccurate, can deliver damage to nearby surroundings, making a direct hit unnecessary.
![explosion](/img/explosion.jpg)

Grenades, Timed Mines, Proximity Mines (terrain mines), grenade launchers, artillery, missiles, and bombs all deal explosive effects.  Where ever the projectile lands (either a direct hit or a near hit, see below), everything within an inch takes full damage.  Anything beyond a inch but within 2" takes one less die roll worth of damage.  Anything beyond 2" but less then 3" takes 2 less die rolls worth of damage.  A Mega Bomb detonated 1" away from a mini-fig.  The mini-fig takes the full 3d10+2 damage.  A vehicle that was unfortunate enough to be 3" away takes 1d10+2 damage.  A wall 4" away takes 2 damage, since there are no more dice to be taken away but there is still the modifier.  An inch beyond that desolves any modifiers.

Cover may be used to shield units from explosive damage.  A Level 6 missile detonated 1" away from a wall.  A Standard Trooper was 2" behind the wall.  The wall takes 6d10+3 damage, but since it's shielding the trooper, she takes no damage.  However, if the wall was destroyed by the missile, then the trooper takes 3d10+3 damage.  All cover must be taller then the object being protected for it to be considered cover.

Mini-figs suffer from knockback effects.  A mini-fig will be knoked away from the explosion one inch for every die roll he took in damage.  Seeing corpses of soldiers fly by, hehe.

# Near Hits

Yes, weapons fire that misses do go somewhere.  For ease of play, energy weapons and non-explosive projectiles go off into the abyss somewhere.  However, explosive shells do land somewhere, and explode.

When you miss a target when firing an explosive weapon, decide how much you missed by.  Check what number you rolled and the required number, and find the difference.  That number is how many inches you misses by.  Rolling a 4 by firing a Bertha Artillery means you missed by 4" at Long Range.  Where the explosion emits from is decided by the player who owns the target at hand.  It must be within a 45 degree firing arc of the weapon.  So that target would take 2 damage.  Whoppy.

# Fire Effects

Some weapons deliver fire damage.  Fire deals little damage at a time, but anything on fire can continuously take damage and allow the fire to worsen, where the fire can spread to nearby units and grow to a stronger state.
![minifig on fire](/img/flames.jpg)

There are three types of fire damage:  1d6, 2d6, and 3d6.  This represents how much damage the target takes.  The damage on any fire weapon is how much fire damage the target gets.  The turn after the target gets hit during the Special Phase, roll the necessary fire damage.  If it is at most 1/3 of the maximum possible damage, the fire damage goes down by 1d6 (if it is 1d6, the fire goes out).  This is a 2 or less on a 1d6, a 4 or less on a 2d6, and a 6 or less on a 3d6.  These rolls continue turn after turn either until the fire goes out or the unit is burned to pieces.

Fire can also grow.  If the target has been on fire for five straight turns, the fire damage increases by one, to a max of 3d6.  Fires can also spread.  If the unit on fire has been 3" or less away from another unit for one full turn, the other unit gets on fire with 1 less die in fire damage, to a minimum of 1d6.

Fires can be put out numerous ways.  A Fire Extinguisher is the easiest way.  However, if the unit goes into water, the fire is put out instantaneously.  A mini-fig on fire can roll around in the dirt during the special phase to remove 1d6 of damage, though he can't shoot during the attack phase.

Multiple Fire rolls are not possible.  If a Plasma Flamer hits a specific tank 4 times, either during the course of 4 turns or by 4 rifles in one turn, the tank does not take four 1d6 fire rolls.  However, if a weapon that delivers more fire damage then the target's current fire damage it's taking, the larger one takes effect.

# Vehicle Collisions

Vehicles may collide with other objects if you have no other option left.  Vehicles colliding with objects deal and recieve damage, but it can lead to powerful results.  Damage is done when the collision happens, in the movement phase.  Walkers take and deliver half damage in collisions.
![walker collided by a small car](/img/collision.jpg)

Troopers in the way of a vehicle are in a tough situation.  Due to the high horsepower of the vehicle, the trooper is instantly ran over.  However, any trooper may jump out of the way.  Roll a 1d10 for every trooper who got hit.  If the result is their armor or less, they successfully jumped out of the way, but lose the rest of this turn.  Troopers who are ran over take 1 damage for every 10 area of the vehicle.  Troopers who run into another trooper knocks the other trooper back 1" for every inch they moved.  Trooper collisions against anything else have no effect.

When a vehicle rams into a building or any stationary object, take this into consideration;  The vehicle takes xd6 damage, x being the number of inches moved this turn.  The building takes yd6 damage, y equals the max speed of the vehicle.

If a vehicle rams head on with another vehicle, both vehicles take xd6+yd6.  X is the max speed of the vehicle ramming and y is the max speed of the vehicle being rammed.  Ouch.  A vehicle with a speed of 8" rams into a vehicle with a speed of 13", both vehicle take 21d6 damage.

If a vehicle rams into the vehicle on the side, it is calculated like so;  the rammer gets xd6 damage and the rammed gets yd6.  X equals the max speed of the rammed vehicle, and y is the rammer's armor after it takes the damage from the collision.

A vehicle doing a rear ender on another vehicle is done yet another proccess.  The rammer gets (rammed vehicle's max movement minus the inches rammer moved this turn) times 1d6 in damage.  The rammed gets (ramming vehicles armor after ramming) times 1d6 in damage.

