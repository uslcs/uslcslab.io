
Book 3: Division Level Combat
Previous - Civilians   Next - Example Nations

Pirate Factions

Not all who ride the galactic winds are good.  Some are evil, or at least everyone thinks they're evil except themselves.  They are the galactic pirates, who roam the galaxy in roving fleets, searching for jobs, money, or just plain destruction.  Their fleets sometimes can be simple ragtag fleets armed with several squads of mates, while others can be an armada of battleships with hoardes of troopers.  Unlike the nations who are to conquer the entire galaxy, or the corperations who make money through trade, or the civilians who are keeping to their peaceful lives, pirates raid outposts for money, death, or for fun.

Pirates cannot have any ground vehicles.  The need to transport a tank from planet to planet is hectic.  They usually don't have enough resources to construct a ship to carry ground vehicles, and if they do, why don't they spend it on fighters, then?  Artillery weapons may be equipped on flyers, for these gangs have replaced some space with a recoil prevention device, which decreases damage by -2 to all artillery (except on any type of base and a flying mobile base).

Pirates do not sit around all day in between missions looking out in space.  They hang out in the bar, having many fights, arm wrestles, and so on.  All close combat damage have a +2 bonus when the ship mate attacks.

There are, however, two new vehicles allowed in the pirates' arsenal.  They are the motorcycle and the guncraft.  The motorcycle is the only ground vehicle allowed, and it is usually used to soften up the enemy's forces or to rush in, take the booty, and rush out.  Guncraft are the more common types of vehicle.  It can carry up to five troops, with the fifth troop causing a -1" penalty.  This craft may have up to two of the smallest weapon possible.  They are designed to move squads through the air.
 
Name 	Move 	Armor 	Points
Motorcycle 	12" 	8 	10
Guncraft 	15" 	15 	25

Motocycle can carry 1 mini-fig at no penalty, and may mount infantry weapons at half penalty.  An Auto Photon and a Grenade Launcher imposes a -2" penalty, instead of a -4" penalty like normal.  Motorcycles can be carried on flyers at a -3" penalty or on a mobile base for a -1" penalty.  Guncraft are flyers, and troopers on board get no penalty for firing. Like motorcycles, they may be carried on flyers for a -5" penalty and on mobile bases for a -2" penalty.  Both vehicles do not need additional locomotion.

Pirate Troopers

In addition to using flyers and motorcycles in their armies, Pirates also have troopers.  These troopers usually attack from Guncraft, although also they do ground assaults and guard bases and such.  They don't like the ground too much, though sometimes they have to in honor of their fleet captain.  Their names may reflect off of a ship worker.  Most of the time they are working on their flagship, or a smaller ship, except during the short times of battle.
 
Name 	Standard Ship Mate
Type 	All Purpose Trooper
Move 	6"
Skill 	1d6
Armor 	4
Cost 	5

These guys form the bulk of a pirates' trooper force.  They are basiclly a modified version of the Standard Trooper, a more faster but lighter armored trooper.  Watch out for these guys, becuase if you can't gun them down fast enough, they'll be on your throat!
 
Name 	Navigator
Type 	Fleet Director
Move 	5"
Skill 	1d6
Armor 	4
Cost 	10

The Navigator is the mini-fig who plots courses in which fleets may follow.  He may drive vehicles, but that is not his specialty.  He can not only plot fleet movements, but he, like the scouts of the national armies, can direct artillery and missiles.  He, like the scouts, may be within 12" of a target, and all artillery and missiles may fire at that target in 60" using their Long Range requirement and with a +2 modifier to the skill roll.  At least one navigator must be in play unless the pirates have a base on the battlefield, or else your gang does not know how to get home after the battle.
 
Name 	Skydropper
Type 	Base Attack Trooper
Move 	5"
Skill 	1d6+1
Armor 	5
Cost 	10

Skydroppers are essentially the main attack unit for attacking bases.  They are similar to the Marine infantry of the nations, except of several things:  They may jump out of a flyer if it is 4" above the target with a half movement penalty.  This prevents the needs for anti-shock boots or superthin parachutes, which are described below.  In battles, the Standard Ship Mates along with the flyers provide cover fire to the guncraft zipping toward the enemy base, then dropping the Skydroppers, in which they would either come in, kill all the gunners, and use the base guns to wipe out the rest of the army, or to raze the base from the inside out.
 
Name 	Suicidal Droid
Type 	One-time Attack Droid
Move 	7"
Skill 	1d8-1
Armor 	5
Cost 	10

Pirate gangs do not have the resources to maintain an android army like the nations and corperations, who have unlimited money to use in repairs and upgrades.  So they build special androids, or modify captured versions.  These androids are a little more frail and clumbsy, but have lightning speed.  They have a bomb imbedded in their heads, and whenever their vital systems shut down, like from a weapons blast, it detonates, causing a 2d10+3 explosian.  You may have Suicidal Drones shoot themselves in this manner.  Unlike other androids, they do not abide by the android generic rule, and they do not have to be in communication, except at the beginning of the game.
 
Name 	Technology Fixer
Type 	Vehicle Repair/Android Control
Move 	5"
Skill 	1d6
Armor 	4
Cost 	12

Tech fixers are more crude forms of the Mechanic.  They do the same effect that a Mechanic does, with a 1d6 armor heal instead of a 1d10.  However, they may capture disabled androids.  Any android that suffered from the effect of an Artificial Intellegance Failure Device may be revived by a Tech Fixer, both friend or foe alike.  He must make two skill rolls; the first is his skill roll, the second is his skill roll with a -1 modifier.  If the second roll is higher, the android is revived under your control.  This is done during the special phase.  A Tech Fixer may also modify a captured android with the bomb device.  This is an automatic success, and it is done during the Special phase.  The droid becomes a suicide droid, in which it's stats change to match the suicide droid's stats.  If it's original armor is less then 5, however, it stays there.  Sweet, huh?

There are three new items pirates can use.  They are the Superthin Parachute, Artificial Intellegance Failure Device (AIFD), and Bandages.  Bandages cost 1 point each, and instantly heal one armor during the special phase.  However, they may not be used again.  The parachute replaces the need for Antishock boots.  They cost 4 points, and causes the pirate to land at the end of the attack phase.  However, the pirate may shoot at targets with a -1 to his/her skill roll in any direction, instead of in front of him.  Other troopers may fire at him for a -1 penalty too.  Unlike Anti-Shock boots, they are a one time use.  AIFDs cost 5 points, and look like a stolen suitcase.  If any A.I., android, or anything that is computer operated comes within 5", they automaticlly die, but they are not removed from the battlefield.  This is for the Tech Fixer, so he/she can take over downed robots.

Previous - Civilians   Next - Example Nations

