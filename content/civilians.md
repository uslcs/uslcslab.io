---
title: Civilians
---


Book 3: Division Level Combat
Previous - Corperation Troopers    Next - Pirate Gangs

Civilians

In the middle of all this conflict, there are the peaceful citizens of all the towns and hamlets of this universe.  However, they must fear drafts into their local army, battles nearby, or even invasion.  How do they cope with all of this?

Over the years, whenever a fight occurs near a town, the citizens react appropratly.  Their gun masters act as snipers, their doctors drive ambulances around, making sure everyone is in good health.  Civilians can also rally behind an army and act as support.

There are several global rules for civilians.  If you play them, you are at a statistically huge disadvantage, thus civilians do not abide by points.  They do, however, abide by ratios to keep the number of super units down.

DO NOT play one on one with any one else.  Civilians are designed for four player games and up, maybe three if you want to push it.  I recommend one civilian player with six players or less, and two if you have more then six players.  Absolutly do not play one on ones.  The other player (unless (s)he's a civilian also) will ultimatally win.

You are neutral.  No one may fire on you (except by stray explosions) and you may not fire on anybody else, until YOU attack them.  Other nations may use diplomats to use your masses to achieve victory.  If you are willing to do this, then fine, since it is the only ideal way of winning (unless you are up to an all out attack against lines of tank stoppers, grenades, and plasma gun shots).

There may be two weapons that the Gun Expert can use out per Gun Expert.  Five Gun Experts allows ten pistols.  One Close Combat weapon is allowed for each Athletic Mini-Fig.  Other weapons are supplied by allies or stolen from dead troopers.

If you are against a civilian player, your objective is to capture the main computer in the town hall.  Once you do that, the civilians are completly under your control, so the civilian player essentially loses.

Civilians do not abide by communication.  However, your 'squads' must be of four or more.  For every number less then four in one squad, the squad members recieve a -1 penalty to their skill rolls and have the same effects as no communication.  Orbdrops are not allowed by civilans, but skydrops are.

Civilians
 
 
Name 	Casual Mini-fig
Type 	Innocenrt Bystander
Move 	4"
Skill 	1d4
Armor 	2
Ratio 	Standard

These are the common folk of a town.  Either if they are the local gang, the preppies of a school, or whatever, these are what a usual civilian force is made out of (I laugh at the 'force' part).
 
Name 	Gun-Expert
Type 	Hunting Civilian
Move 	4"
Skill 	1d6
Armor 	3
Ratio 	1 for every 3 Standard

These guys are your usual hunterrs of game.  They are good shots, and are the civilians main weapon.
 
Name 	Athletic Mini-Fig
Type 	Gym Lover
Move 	5"
Skill 	1d6-1
Armor 	3
Ratio 	1 for every 4 Standard

These are your body-builder dudes.  They can wield close combat weapons and smash your enemy to pieces.  If the armies do not have one tank guarding one of their close combat soldiers.
 
Name 	Doctor
Type 	Needle-Poker
Move 	4"
Skill 	1d6-1
Armor 	2
Ratio 	1 for every 5 standard

The standard medical doctor.  Exactly like a national doctor except without the armor.
 
Name 	Police Officer
Type 	Security Force
Move 	5"
Skill 	1d6
Armor 	3
Ratio 	1 for every 5 Standard

Okay, your hunting maniacs aren't good enough for an onslaught?  Well then, thanks for insulting my creation, but here are some extreamly good mini-figs for your own use.  Use your police force with caution.

Towns

In version 1.4, there were all these rules for constructing a town.  For the new updated version, it is now quite easy to construct a town in which your civilians live, die, and be born (which the latter is impossible to do in the middle of USLCS, but hey, if you want that to happen, fine).  Your number of buildings must be suffiecient to house the number of civilians you have.  You may have one or two 'abandoned' buildings, but that is all.  You must have one City Hall and have up to one hospital.  A hospital acts like a medical bay for the military counterparts.  The City Hall is where the main computer is.

You can also have mechanic bays.  Civilian mechanics have the same stats as their counterparts with a 1 to 6 ratio.  There is a limit of one mechanic bay for every two vehicles you have.

You can only have level one weapons on your City Hall in the beginning, and they must be lasers or artillery.  You may, however, have a Mechanic automatically place a weapon stolen from somebody else onto any building.

Vehicles

Civilian vehicles are constructed exactly the same, except there are a few rules and limitations that you, the civilian player, must follow:  You can only have treaded and flyer vehicles.  Vehicles' armor is divided by two twice instead of once.  There can be one medic vehicle (ambulance) for every 'medic bay' you have in your hospital.

Weapons are not allowed on vehicles in the beginning.  However, mechanics may automatically put a stolen weapon on any of your vehicles, except an ambulance.

Previous - Corperation Troopers    Next - Pirate Gangs

