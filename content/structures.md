
Book 2: Battalion Level Combat


Previous - Vehicles and How to Build Them    Next - Vehicle and Base Weaponry

Bases and Buildings

At the heart of an army, there is an essence, an essence that is one of the very few things that a trooper may love.  It is his or her base, where (s)he lives, works at, and defends, if needbe.  A base is a great threat to any enemy, for it is like a medieval castle;  the base controls the land.  Where there is a base, there is the nation.  And when there's a nation, there is trouble.

When building bases, or other types of structures, you can have two things in mind:  is it a small bunker or a large command center?  Bunkers are generally small, under 800 points in cost, and has a lot of firepower.  Command centers are huge, at least 1000 points and can reach up to 2000 maybe 3000 points.  Command centers are not just large bunkers;  they have dorms for the garrison stationed there, there is a briefing room for commanders to brief their troops, there is the garage which houses vehicles, and of course the command center, where the weapons and other base functions are operated from.

When constructing a base, you must decide what type of base you should have;  a traditional ground base or an exotic sky base?  Steps in building each is described below.

Ground Bases

Ground bases are the commonplace structure, ranging from signal story bunkers to sprawling complexes several stories up and many more underground.  Ground bases can be fun, but make sure you can defend it from the onslaught of your opponite, for he will always outnumber you.

First off, you must find the armor and point cost of each individual wall.  If there is a second floor in which it's wall is connected with the wall underneath, count it as a second wall.  Find how long the wall is in dots, multiply that by how many dots thick the wall is, and finally multiply that number by how many bricks tall the wall is.  Divide that final number by two to get the final cost and armor of the wall.  This may be costly, but it is very defensive.  Underground floors do not have walls, see below.

Once you have decided the above ground walls, place any of the interior walls inside the base as well as floors and the celing.  Walls and floors may only be blasted apart by a singal hit of a vehicle weapon for ease of play;  damage of the celing is divided equally among the walls that connect to the celing.  These are all free.

Underground levels cost 200 points a level, and can be as big as you want.  For ease of play, you can have underground floors elsewhere off the battlefield, so you don't have to make an elevated surface so you can have your underground bunkers.  Underground floors may not be destroyed, but they may be entered from a burrowing vehicle, see Burrowing Vehicles.

Stairs and doors cost 10 points each.  All bases start out with one free door, but extra doors cost points.  Large doors meant for vehicles cost 15 points, and the vehicle must be able to get out this door without any trouble.

Computers cost 2 points each, and allow the mini-fig operating it to remotly control any one weapon or weapons on a base, see Operating Weapons.

There are things called Enhanced Base Functions, formally called Outstanding Architectural Features, or 'Cool Stuff'.  These are several devices to enhance the functions of a base.  They normally cost 25 points, but some may cost more.  These are a few examples of Enhanced Base Functions, but probably you can come up with some ideas.

Elevators - Instantanious access to any level on the base;  uses one movement phase to use.

Special Doors - These doors are designed to save space.  Doors like these include rollup doors and sliding doors, and others.

Booby Traps - Described below;  depending on the deadlieness of it may cost more

Pop-up Weapon - Door or some other device that allows a hidden weapon to suddenly appear;  may be considered a booby trap.

Sky Bases

Construction of a sky base is different.  First off, find the area of the sky base.  This is the armor of the base.  If you want, you can have several sections of a sky base have it's own armor, so when one is destroyed only a small chunk is knocked out.

Add 20 points to the cost for every 100 armor the base has.  This represents the hovering jets needed to hold up such a weight.  As before, computers cost 2 points.  There should be little or no walls of a sky base, and it may only be single floored.

Only one vehicle (obviously a flyer or gravship) may be in the base at a time.

If you are willing to put up with it, you may decide on how much a sky base may carry.  Find the total area of the base, and divided that number by 20, rounded down.  That is how many movement penalties the sky base can take and still carry it's weight up.

Some Enhanced Base Functions may no longer be appropriate for sky bases.  Thus, a new set of them has been listed for your convience.  As before, they cost 25 points, unless otherwise listed.

Express Elevator - Allows instant travel by any number of mini-figs from the ground to the base and vice versa.  This movement uses up the movement phase.

Booby Traps - See below.

Retractable Catwalk - This allows you to seperate two points of the sky base from access by ground forces.

Seperation Conncetors - Allows seperation of the sky base into two seperate bases.  Costs 30 points.

Booby Traps

Booby traps are very deadly, especially when your opponite is capturing your base.  They can trigger mines to explode, weapons to fire, doors to open, and releasing some deadly inhabitants of the planet, such as the Egaphant of Triscit 6, who looks like an elephant but really a ticked off polar bear.

First off, all booby traps cost at least 25 points, for they are an Enhanced Base Function.  Then there are the weapons involved, such as a grenade or Super Lasers.  These add to the cost of the trap.  Then you must decide if the trap is manuel or not.  If it is automatic, it activates when any mini-fig, friend or foe, crosses the area of the trap.  Manuel control is by a seperate computer (which costs 2 points) and is activated whenever the owner of the base feels like it.  That is, the owner of the base.

In the case of a trap doors leading to a pit of spikes, just use a bunch of close combat weapons, such as Power Knifes or Thrust Spears.

Damaging Buildings

Bases can suffer a lot of damage.  It is difficult to cordinate such an attack, because all bases are armed with the biggest and the deadliest weapons, and you'd have to sacrifice a lot to destroy a base.

First off, when a wall of a ground base reaches zero, the last place the blast hit will result in a breach.  Knock off a chunk of the wall, but not the entire wall.  Other chunks will just need a direct hit to force down.  The rubble from the wall will fly the direction of the gun fire, and results in a 1d10 non-explosive damage to all mini-figs in 4" on the opposite side of where the wall was from the enemy that fired the shot.

When the sky base, or a piece of the sky base, is destroyed, crumble it up and sprinkle the pieces below the base.  If there are mini-figs below the falling debre, they too take 1d10 damage roll.  Looks like there could be some casulaties.

Capturing Bases

One of the most beneficial parts of attacking a base is capturing it.  Bases are already tough to destroy, but this is even tougher.  You either have to make a wide enough gap in the wall to allow your forces to enter, or to somehow get in one of the doors.

All bases have a free main computer, placed whereever the builder wants it.  This is the main control hub of the base, and is where the most defenses are.  A player in control of the main computer can operate all the doors, fire all remote controlled weapons, and operate all Enhanced Base Functions.

To capture a main computer, you must get either a commando, technition, or a Tech Fixer up to it.  During the Special Phase, roll the mini-figs skill roll.  If it is a 6 or higher, base control is handed over to the player who owns the mini-fig at hand.  Only one mini-fig can try once a turn, but you may have multipal mini-figs you may try each of them to do it.

A main computer may never be destroyed, even if all the walls are burned down and blown away.

Mobile Bases

A type of weapon used in offensive and defensive that's importance is growing as newer technologies can allow it is the moveable base.  A base that can move has the advantages of moving from battlesite to battlesite, escaping from a rampage, and better support for troops on the front line.  However, their price tag is high, limiting their use and keeping their size small.

A mobile base is constructed very much like a regular base, except you add locomotion to the base.  Add up the armor from each of the walls, combine them, then divide them by two.  This is the minimum requirement to power the base's movement.  Once you have slapped on the locomotion needed to move this behemoth, it's movement is cut in half from the regular movement because the requirement was cut in half.  This essentially keeps the amount of locomotion and cost down so that the player doesn't have to spend as much to just slap down a moveable base.

Unlike vehicles, movement penalties do not count on mobile bases.

Flying mobile bases can stand still, much like a gravship.

Previous - Vehicles and How to Build Them    Next - Vehicle and Base Weaponry

