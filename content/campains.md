---
title: "Campains"
---

Book 5:  Army Level Combat

 
Previous - Example Nations

Campaign Rules

Congrats, my dear friend.  You are almost done with the USLCS manual!  But there is still one more book to follow, and that concerns developing your own campaigns.  Campaigns are 100% optional, and is not really recommended unless you have a set of players that you can play with at anytime.  I see that you do, so read the last of this manual!

Campaigns are much like what you would see in real-life, such as Operation Barborssa (sp?) in World War II.  In that campaign, the Germans were attacking Russia and their goal was to conquer Moscow to surrender.  Their tactic was using their infamous Blitz, where they come roaring in and make a slaughter house before the enemy could respond.  A good campaign would include all those attributes.

A campaign in USLCS is a series of battles tied in together that eventually leads to an ultimate victory over one player or the other.  There is always at the beginning of a campaign an attacker and a defender.  The attacker has a particular plan in mind, such as in the above where the German's plan was to take over Moscow.  The defender's objective 'should' be to drive the attacker of their territory out or something to that extent.  A good idea is to draw a map showing, say, a location that the attacker must capture, where the attacker's forces first come in contact with the defenders, and special locations such as bunkers, bridges, mountains, and the like.  This will greatly help with the flow of the campaign.  Whenever the two opposing forces meet, a battle starts.

Each battle has it's own objective, like:  Destroy enemy scout force, raid bunker, etc., or the defender of the campaign may have a counter-attack, in which the defender is on the offense during the battle.  This is much like independent battles.  The first battle is entirely like a standard skirmish that you might of played already, but that first battle decides what the next battle will be like, and so on and so forth, until one side winds the campaign.

An example campaign:  Side Red's objective is the destroy Side Blue's command and control center on the peak of Mount Thangman.  Blue's objective is obviously to prevent that and to destroy the attacking force.  Red decides to remove the bases' recon units patrolling the base of the mountain, so they can't be sighted.  Red destroies Blue's scouts, and moves in up a hill.  They then come in contact with Blue units that have been waiting for them, and suffer losses, forcing them to retreat.  The only other way for Red to get up the mountain without calling off the whole assault is through a pass guarded by a bunker.  They attack and destroy the bunker.  Three battles have been fought so far.  It is the player's decisions of what the outcomes of each battles does in the campaign, which ultimatly plans out the future battles.  I hope you understand me now.

Campaigns aren't just a sequence of battles.  Casualties on one side carry on to the next battle.  Troopers grow smarter as they fight battle-to-battle.  These can easily be accomplished in an environment such as a campaign.  Let me explain these concepts more fully.

When a battle is won, the surviving units may be carried over for the next battle, or kept out if you want to save them for later.  If you want additional units, you can't just set a point limit and increase your armies to match.  Before the campaign, the two sides must set up a value called 'Reinforcement Points'.  This is the number of points you can add on to your armies at the end of each battle.  Point limits have an effect only on the first battle.  This allows for much more strategy to be involved in each battle, instead of just running forward and losing many lives but killing them off, you must make it so that you can do the most amount of damage while taking the least amount of casulaties.  This also promotes retreats and other ways to end a battle while saving units.  A good commander can at the final battle have an army worth 2000, where the defending player has 500 points, the amount of reinforcement points allowed after each battle.

Troopers can get smarter and tougher.  This is called Character Development, and is centered around heroes.  If a trooper has survived four battles in a campaign, they become a hero with 5 free points that do not count in your reinforcement points that may be used to spend on the extra stats.  If a hero survives each battle, they get one free point that does not count in the reinforcement point total that may be used to add on an ability, or saved up to buy a big ability.

Campaigns may also be tied into other campaigns.  Once a campaign is over, you may use the units you have in a fight in another campaign, and your troopers still keep the number of free points they have for surviving battles.  Campaigns may also decide what the next campaign would be about, like; the first campaign is to take over Green's Congress.  The second one is to get rid of the rebellions that sprout up because you have taken over their country.  And a third campaign may be to fend off an invasion force from a different world.  Multiple battles tied together is a campaign, multiple campaigns is a war.

This concludes the Version 2.0 rules for USLCS.  We hope you enjoy playing this game, with all it's wonders and concepts and plain fun.  Happy Gaming!

Previous - Example Nations

