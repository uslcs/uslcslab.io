

Book 2: Battalion Level Combat


Previous - Bases and Structures    Next - National Troopers

Vehicle and Base Weaponry

Infantry weapons are good for clearing away a field of troopers or troop transports, but for blowing up other vehicles and bases, bigger weapons must be used.  This document explains how to work weapons and the different types of weapons.  Some weapons may only be mounted on bases.

There are two ways to control a weapon;  via remote control or manually.  Manual takes precedence over remote control, so if a mini-fig is at a gun, and another is away at an computer, the one at the computer may not fire if the one at the gun is firing.

A mini-fig at most one inch away from a gun may control it manually.  He/she may fire as many guns manually that she can 'touch' as long as each fires at the same target.  All guns fire at the same time in this case.

Remote Control is by a computer, which costs 2 points.  One mini-fig may shoot as many guns remotely as (s)he likes, as long as they all fire at the same target (and all fire in the same round).  A mini-fig cannot control a manual and a remote controlled gun at the same time.  If you want to shoot at two targets, you'll need a second mini-fig, and that means a separate computer if you are firing by remote.

Lasers

Lasers are one of the most commonplace weapons found in the galaxy.  They have a relatively low cost, are accurate, and powerful, but are limited by range and the inability to shoot over objects.  However, once within range, lasers pose a serious threat to any vehicle or base.  In addition, they have unlimited ammo, so you will never run out of shots.
 
Laser Name 	Short Range 	Long Range 	SR Skill 	LR Skill 	Damage 	Move Penalty 	Points
Small Laser 	10" 	20" 	3 	5 	2d6 	-- 	10
Large Laser 	14" 	26" 	3 	6 	3d6 	-1" 	15
Super Laser 	16" 	30" 	3 	6 	2d12 	-2" 	20

This table follows the same stats as the ranged infantry weapons.  Move penalty apply to the vehicle only.  Lasers are great against other vehicles and bases, but are not that great against infantry, for they can only shoot one at a time.

Artillery

Artillery is one of the oldest weapons still used in this era of conflict.  Their range is long, can shoot over objects, have explosive effects, but cost more then lasers and are slightly weaker and very inaccurate.  Even though they are projectile weapons, artillery have unlimited ammo, because essentially they have allot of shells in them (if you want to keep track of the dozens of shells, go ahead, but we don't want you to).
 
Name 	Short Range 	Long Ranged 	SR Skill 	LR Skill 	Damage 	Move Penalty 	Points 	Length
Small Artillery 	15" 	30" 	4 	6 	1d10+2 	-1" 	15 	5
Large Artillery 	20" 	40" 	5 	7 	2d10+2 	-2" 	24 	8
Bertha Artillery 	30" 	55" 	6 	8 	3d10+3 	-3" 	30 	12

Artillery cannons have a 'length' to them, as shown on the table above.  This is how long the barrel is, in dots.  Remember to consider near hits and explosive effects when firing artillery.

Missiles

Missiles are expensive, but have a longer range and greater firepower then any of the other weapons.  However, unlike the other weapons, missiles are constructed from scratch.  A Level 1 missile will have the stats below.  A Level 4 missile will have the stats times four.
 
Name 	Short Range 	Long Range 	SR Skill 	LR Skill 	Damage 	Move Penalty 	Points
Missile 	5" 	10" 	1 	1 (+1) 	1d10 	-1/2" 	5

A Level 1 missile will just be a cone piece as shown above.  A level 2 missile will be two cone pieces.  A Level 4 missile would be a large cone piece, and a 6 missile would be a large cone piece with two round 2x2s below it.

For example, a Level 5 missile would have a 25" Short Range, 50" Long Range, 5 SR Skill, 6 LR Skill, 5d10 damage, a move penalty of 2 1/2", and costs 25 points.  Even though they can fire in any direction and have explosive effects, missiles, once used, cannot be used again.

Bombs

Bombs are explosives dropped by flyers over the enemy fortifications.  Like missiles, bombs can only be used once, but is cheaper, more accurate, and more deadly then artillery, which flyers cannot mount.  However, bombs can only shoot down, maybe at a small angle.  They are fired in the movement phase, and can be dropped on any enemy target in the path of the flyer or gravship.
 
Name 	Skill Requirement 	Damage 	Move Penalty 	Points
Mk. I Bomb 	3 	1d10+2 	-1/2" 	5
Mk. II Bomb 	3 	2d10+2 	-1" 	8
Mk. III Bomb 	3 	3d10 	-1.5" 	10
Mk. IV Bomb 	4 	3d10+5 	-2" 	12
Mk. V Bomb 	4 	4d10 	-2.5" 	15

Bombs are made like domes.  A small dome equals one Mark, so two small dome equals a Mark II Bomb.  A large dome equals two marks, so two domes equal a Mark IV.  Two large domes and a small one equal a Mark V, and so on.  Bombs may only be mounted on flyers and gravships, and once used, the bomb is gone.

Flamethrowers

With all these big guns that cause massive destruction, there is really no defense against hordes of infantrymen.  That's why scientists have made a larger more destructive flamethrower mounted on vehicles to wreck havoc on infantry.  These guns are even good against vehicles and bases!
 
Name 	Short Range 	Long Range 	SR Skill 	LR Skill 	Damage 	Move Penalty 	Points
Petroleum Flamethrower 	4" 	10" 	2 	5 	2d6 	-1" 	12
Methane Flamethrower 	5" 	12" 	2 	6 	3d6 	-2" 	18

Flamethrowers, like you would expect, deliver fire damage.  However, if any trooper is in between the barrel and the target or one inch away from the flame delivery, they too take the damage.  Very useful for infantry destruction en masse.  Flamethrowers do have unlimited ammunition.

Note:  These next weapons are for bases only.

Chaos Guns

During the invasion of Nimbus 7, the Garn Guild needed a way to defend it's territories from large number of armies.  So they utilized a chaos gun, a giant plasma based weapon that delivers great fire power over long distances, capable of taking on tanks miles away from the base.
 
Name 	Short Range 	Long Range 	Skill SR 	Skill LR 	Damage 	Points 	Move Penalty 	Armor
Small Chaos Gun 	40" 	80" 	3 	6 	5d10 	40 	-4" 	40
Medium Chaos Gun 	50" 	100" 	4 	7 	6d10 	50 	-6" 	50
Large Chaos Gun 	60" 	120" 	5 	8 	8d10 	60 	-8" 	60

Unlike other weapons that deliver d10 damage, Chaos Guns do not deliver explosive damage except when it hits a target.  A target when hit is thrown back 1" if it survives the onslaught.  A Small Chaos Gun can fire every turn, a Medium can fire every other turn, and a Large one can fire every three turns.  Only one Chaos Gun may be on one side in a game, and only ground bases and tracked bases may wield these mighty weapons.  You must decide the turn before if you are going to fire the Chaos Gun, so declare so during the attack phase.  You must shoot the gun the next turn.

Anti-Matter Missiles

Sometimes a missile and artillery barrage isn't enough to destroy a well defended base.  So an allied base on the other side of a planet could fire an anti-matter missile at this base and wipe it clear off the map.  Like regular missiles, anti-matter missiles are custom built, but must be placed in a silo.  Silos cost 30 points, and may fit as many missiles as you want.  Silos must be next to or inside a base for it to function.  Access into the silo is impossible, so don't worry about commandos deactivating your missiles.

A Level 1 anti-matter missile is identical to the Level 6 conventional missile.  However, when the missile hits it's target, see below, it detonates in a 5" explosion radius.  This time, anything in this radius is destroyed, including your own men, so watch out.  They cost 50 points.  A Level 2 anti-matter missile costs 25 more points, looks like a Level 7 missile, and has a 10" explosive radius.  Every level you go up, the radius increases by 5" and the cost 25 points.

Anti-Matter missiles may only be targeted by scouts, who are described in Book 3.  When scouts get within 10" of any target, their owner may choose to fire the anti-matter missile from his/her own base during the Special Phase.  It takes one full turn to hit it's target, which is an automatic hit.  Make sure your scouts run fast.

Silos can only be on ground bases and mobile air bases.

Vehicle and Base Equipment and Weapon Enhancements

Weapon Enhancements have been used to increase the quality of weapons, yet they are rarely used anyway.  Vehicles and bases may have 'equipment', similar to the troop portable version.  These are used mainly for the weapons of the vehicle/base, but some are used for the vehicle/base itself.
 
Name 	Points 	Function
Wheel Springs 	12 	Half damage in collisions to vehicle
Mecker Shielding 	15 	Prevents weapon blast/mini-fig from entering room/ armor of 20
Smart Bomb System 	8 	Allows bombs to travel 5" in any direction when fired.

The table is easy to follow.  I doubt I should explain it.

These equipment do not have to be represented on the battlefield, but you should tell your opponite what you have when the time is necessary.

Whenever a big weapon or equipment is hit, it automatically is disabled, until a Technition, see National Troopers, can fix it.  This is only a direct hit, and not by an explosion nearby.  Chaos Guns, however, have armor.  When the armor reaches zero, it is disabled.  When an Engineer reactivates it, the armor goes back up to half of what it's previous maximum was.  A large gun would have 25 armor after the first time it is fixed, then 13 (rounded up) after the second time, 7, 4, 2, and so on.

Gun Mountings

Vehicle weapons, as is, can only fire in a straight line, and has no access to the other two axis (no pun intended).  For five points, your gun(s) may be mounted on a turret or hinge, for a grand total of ten points if you use both mountings.

Turrets allow the weapon to turn in any direction that you desire, and probably the most important mount.  Hinges allow the gun to flex up and down.  Hinges are only needed for lasers, flamethrowers, and Chaos Guns, for artillery come with one automatically, missiles can direct themselves, and bombs, well, go down.

Previous - Bases and Structures    Next - National Troopers
