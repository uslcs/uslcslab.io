
Book 2: Battalion Level Combat

# Vehicles and How to Build Them

The firepower of handheld weaponry is far too weak to deliver enough damage to destroy an enemy.  Troopers are far too slow to move quickly around the battlefield.  Now the deadliest weapon known to plastickind will be introduced in this chapter: Vehicles.

There are four different types of vehicles:  Treaded, Walker, Gravship, and Flyer.  Treaded have been around the longest, being used for tanks or missile delivery vehicles.  Treaded move the slowest, but provide the most armor to the vehicle and can carry a lot of weight on it's shoulders.  Walkers is another ground vehicle, this time utilizes speed but can't carry alot of weight and has little armor.  The two airborne vehicles are flyers and gravships.  Flyers is like the traditional jet, and is the fastest vehicle but cannot stand still.  Gravships are the modern equivelent of helicopters or harriers.  They are slower then flyers but have the advantage of standing still and vertical takeoff and landing.

!(flyer)[/img/Pc250034.jpg] !(gravship)[/img/Pc250032.jpg] !(walker)[/img/walker.jpg] 
Flyer                                                                        Gravship                         Walker
(No treaded photo)

Vehicle construction is very customizable.  To find out what it's Armor and movement is, decide the base vehicle cost and locomotion.  The base vehicle cost is the area of the vehicle divided by two.  This is how much points and armor the vehicle currently has.  Now the vehicle needs to move.  Here are the different sets of locomotives:
 
|Treaded Locomotive Name |	Points 	| Pull | 	Speed | 	Added Armor |
| ----------------------|---------------|-----|--------------|---------------------|
|Small Wheels| 	5 |	12 Armor |	10" |	5 |
|Medium Wheels| 	10| 	18 Armor| 	10" |	10|
|Large Wheels| 	15 |	24 Armor |	10" |	15 |
|Giant Wheel\ 	20 |	30 Armor |	10" |	20 |

 
|Walker Jump-Jet Size |	Points |	Pull |
|--------------------|------|------------|
|Small 	|5 |	5 |
|Medium |	10| 	10|
|Large 	|15 |	15|

 
|Flyer Locomotive Type |	Points |	Pull |	Speed |	Armor Added |
|---------------------|---------------|-------------|--------|-------------|
|Jet| 	10| 	15 Armor |	12" | 	5 |
|Rocket 	15 	20 Armor 	15" 	8
|Super Rocket 	25 	30 Armor 	20" 	10

The above tables list the sets of locomotives each vehicle type has.  You may notice that for walkers there are 'jump-jets' but no locomotives and same with gravships.  That will be described below.

You must have enough pull on a vehicle to make it move.  Pull must equal the current armor of the vehicle.  Once you decide if your vehicle has enough locomotion, then decide the point cost.  After that is done, determine the extra armor added onto the vehicle.  This extra armor does not count for the base vehicle armor;  it does not interfere with determining your pull.  After that, decide the movement of your vehicle.  The locomotive with the highest speed on a vehicle is the movement for that vehicle.

Walkers are constructed differently.  Their locomotives are 'legs'.  Every inch of leg counts as 15" speed, 5 armor, and 20 pull.  Cost of every inch is 5 points.  A walker must have two legs, and if both legs are one inch tall, the locomotives are still an inch tall;  do not combine both legs' height.  Walkers also have jumpjets, if you supply them.  The fundamentals work the same as regular locomotion, except it makes the walker go up.  A walker may jump up 10" into the air if it has enough pull.  Jumping in the air works like troop-portable jumpjets, see Weapon Enhancers and Trooper Equipment.

Gravships, on the other hand, uses flyer locomotion.  However, half of the pull must be divided up by 'stabilizers'.  Stabilizers cost 5 points each, and provide 10 pull and 5 armor each.  However, the top speed of such a flyer is slowed down by 5".  This makes a flyer a gravship.  Even though they lack in speed, gravships have more capabilities then generic flyers.

Extra armor may be added on to the vehicle at 10 points a piece for an additional 10 armor and a -2" movement penalty.  To counter this penalty, as well as all other penalties, multiply the movement modifier by 5 to see how much pull it will take to counter the effect.  Use any leftover pull or add some more locomotion to counteract these penalties.

A vehicle may move faster if it has enough pull.  If the pull doubles the original armor of the vehicle, before adding in locomotive armor or added armor, then the vehicle moves an additional 5".  If the pull doubles the double of the armor, then the vehicle may move another additional 5", and so on.  However, it gets way harder to make a vehicle move faster after you have received the 5" extra once.

All vehicles have communication inside of them, so there is no need for a communication device.  Flyers may noto mount artillery weapons.

Troop Transports

Troop transports are essentially vehicles designed to ferry mini-figs across the battlefield.  Troopers can jump out of treaded vehicles or landed gravships, or leap out of flyers, hoping their anti-shock boots are on.  There is one big rule:  Walkers cannot ferry extra troops.  It is bounded by it's crew.

Normal vehicles can ferry troops.  Treaded vehicles and gravships can carry 5 mini-figs at -3" penalty, flyers can do so with 4 mini-figs at a -3" penalty.  Vehicles designated as troop transports do not cost extra, but are bounded by two weapons maximum.  Troop transports that are treaded or gravships can carry 5 troops at -1" penalty and the same for flyers with 4 troops.

Vehicle Crewmen (and women)

Vehicles are not run just by some essence in space somewhere.  Mini-figs pilot the vehicle, drive her around, fire her guns, to destroy their common enemy.
 
Name 	Driver and Gunner
Type 	Vehicle Crewmen
Move 	4"
Skill 	1d4 (1d6 and 1d6+2)
Armor 	3
Points 	3

Under the skill roll, the 1d4 is their normal skill roll for anything, such as firing trooper weapons and such.  The first number in parenthesis represent the driver's skill roll for firing vehicle weapons and the second roll is the gunner's skill for firing vehicle weapons.

Vehicle crewmen may only use the Power Knife and the Blaster Pistol for trooper weapons.  All other weapons are not allowed.  Drivers are the only ones who can drive vehicles, except heroes with the driving ability, see Heroes.  Drivers, gunners, and any other mini-fig may fire vehicle weapons, except that these two are specifically designed to do so.

Gunners are normally found on bases, firing the defensive weaponry.  However, they are found sometimes on large vehicles, too.

There is no need to distinguish crewmen from standard troopers, because crewmen usually stay inside vehicles and bases and troopers are out on the front lines, blowing each other's limbs off.

Vehicle Movement

In Terrain Effects, the manual discussed on how vehicle movement can be affected by movement.  Well, this talks about how each vehicle may move.

Treaded and Walkers may move forward or backwards at full speed, and must make a smooth turn (not to much pressure on this subject.  Just don't let the vehicle turn so tight that the wheels skid).  Flyers may only move forwards and must be able to make a sharp turn (not too sharp of a turn, where it turns completely around on it's axis), and Gravships may move forward or backwards at full speed and turn on it's axis.

For flyers and gravships there is something called Inertia Effects and Altitude levels, both that can be led for your advantage.  Flyers can only accelerate and decelerate up to 2" a turn.  That means that if one flyer is moving 15" a turn, the next turn it can got down to 13", then 11", and then maybe you want to go up to 12", but it can't skip from 12" to 6".  Gravships have a acceleration/de acceleration rate of 4" instead.  Flyers must go at least 3" a turn;  gravships can stop if you want to.

Flyers and gravships may also have altitude levels.  A flyer may go up or down one brick in elevation for every -1" penalty it takes.  Gravships can go one inch up or down without any movement modifiers, the rest have a -1" modifier.  A gravship that is stopped may go up or down the number of bricks equal to it's maximum movement plus one.  However, if a flyer went up several bricks and moved 4" the turn before, it cannot go 3", 4", 5", or 6" in the current turn.  It still moved the number of inches it would of if it didn't change it's elevation that turn.

Flyers must land on flat ground that has a length equal to half the flyer's current movement or more.  After it touches the ground, it must cut it's movement in half the next turn (the only legal time to do it), moving on the ground.  That next turn the flyer is considered stopped.  The flyer may move on the ground at 4" a turn (or 3" if your flyer doesn't go that fast).  Gravships may land like flyers or land vertically, in which it must land on flat ground.  Taking off is the exact opposite, and when a flyer takes off, it is one brick off the ground.

Damaging Vehicles

Vehicles don't just flip over and die like mini-figs.  They are very destructive when they explode, thus resulting in several things.

First, crumble up the vehicle, and divide the parts up in two (if there were 4 wheels and 2 guns, leave 2 wheels and 1 gun), rounded down.  This resembles battlefield debre, and are the tools needed for mechanics to rebuild vehicles on the line, see Mechanics.  Any mini-fig inside the vehicle are instantly killed, and the vehicle itself generates an xd10 explosion, with x equalling the area of the vehicle divided by 10, rounded down to a minimum of 1.  This explosion starts at the outside armor of the vehicle, not from the center.   Airborne vehicles just have their debree land beneath them, and any surviving mini-figs on the vehicle must suffer the neccessary falling damage roll if they do not have anti-shock boots.

Burrowing Vehicles

In case of heavy combat or for special missions behind enemy lines, a commander may utilize drills to submerge his vehicles, where they are protected, and can move them through any obstecle.  Here is how to build them.

A singal drill costs 25 points.  Drills allows the vehicle to drill inside a hill and go through it, exiting on the other side.  A hinge on the drill allows the vehicle to burrow anywhere, and that costs ten points.  Only treaded vehicles may have drills, and not walkers or flying vehicles.

While underground, a vehicle may steer normally, and moves at half movement.  However, your opponite may not attack your vehicle nor can your vehicle take any damage.  When the vehicle burrows, it happens instantaneously, during the Special Phase.  Where ever a vehicle burrows, pile a couple of bricks where it drilled in and drilled out, to represent the mound involved (just like mole hills, except these are bigger then people!).

Previous - Advanced Gameplay    Next - Bases and Structures

