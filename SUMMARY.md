# Summary

* [Prolouge](PROLOUGE.md)
* Book I Basic Gameplay
    * [Basic Gameplay](src/GAMEPLAY.md)
    * [Basic Trooper](src/basic_trooper.md)
* Book II Advanced Gameplay
    * [Advanced Gameplay](src/advanced_gameplay.md)
    * [Vehicles](src/vehicles.md)
    * [Bases and Structures](src/structures.md)
    * [Vechicle and base weapons](src/vehicle_weapons.md)
* Book III Division Level Combat
    * [National Troopers](src/nationals.md)
    * [Corperation Soldiers](src/corperations.md)
    * [Civilians](src/civilians.md)
    * [Pirate Gangs](src/pirates.md)
* Book IV [Example Nations](src/example_nations.md)
* Book V Campains
    * [Campains](src/campains.md)


