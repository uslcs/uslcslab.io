Authors of Ultimate Space Lego Combat Game

David "croxis" Radford and Justin "Seafroggys" Radford

Authors of Ultimate Space Lego Character Game

David Radford


Others and Acknowledgements

LEGO is owned by The LEGO Group

Text and reference materials

Transit RPG by Fiddleback Production CC BY-NC 4.0

Apocalypse World by D. Vincent Baker and Meguey Baker

[Ironsworn and Ironsworn Starforged](https://www.ironswornrpg.com/) by Shawn Tomkin CC BY-NC-SA 4.0

Dungeon World by Sage LaTorra and Adam Koebel

Stars Without Number by Kevin Crawford

[Cepheus Engine System](https://www.drivethrurpg.com/product/186894/Cepheus-Engine-System-Reference-Document) 
